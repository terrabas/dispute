import {ArgCommand} from "../../../core/Models/ArgCommand";
import {Argument} from "../../../core/Models/Argument";
import {ArgumentList} from "../../../core/Models/ArgumentList";
import {Helpers} from "../core/Helpers";
import {ModDatabase} from "../core/ModDatabase";
import {ModEntry} from "../models/ModEntry";

export class MemberInfo extends ArgCommand {
    constructor() {
        super("memberinfo",[new Argument(true,"username","The current server username of the user")],"Warns a user with given reason and logs it. Also suggest further action if required.");
    }

    execute(message, args: ArgumentList) {
        let result = Helpers.getUsers(message, args.getArgument('username').get());

        if(result.length >= 2)
            return message.channel.send("`Multiple members with username found. This is unexpected.`");

        if(result.length == 0)
            return message.channel.send("`No username given!`");

        if(!ModDatabase.hasEntry(result[0]))
            return message.channel.send("`No moderation database entry for this user yet.`");

        message.channel.send("__**Moderation database file for user '"+result[0].displayName+"'**__");

        let modEntries = ModDatabase.getModEntries(result[0]);
        message.channel.send("Total entry count: "+modEntries.length);

        /*
        message.channel.send({
            embed: {
                color: 0xffff00 ,
                description: "Warning",
            }
        });
        message.channel.send({
            embed: {
                color: 0xff0000,
                description: "Kick/Ban",
            }
        }); */

        modEntries.forEach(function (modEntry:ModEntry) {
            let color = 0xffff00;
            if(modEntry.type == "kick" || modEntry.type == "ban")
                color = 0xff0000;

            message.channel.send({
                embed: {
                    color: color,
                    title: modEntry.reason,
                    //description: credits.description.join("\n"),
                    fields: [
                        {name: "Issued by", value: modEntry.issuedBy, inline: true},
                        {name: "Timestamp", value: new Date(modEntry.timestamp).toDateString(), inline: true},
                    ]
                }
            });
        });

        message.channel.send("> Command processing complete");

        return;
    }
}