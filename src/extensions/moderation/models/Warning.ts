import {ModEntry} from "./ModEntry";

export class Warning extends ModEntry{

    constructor(warnedUser:any, byUser:any, reason:string) {
        super();
        this.type = "warning";
        this.issuedBy = byUser.username;
        this.issuedByID = byUser.id;
        this.issuedFor = warnedUser.username;
        this.reason = reason;
    }
}