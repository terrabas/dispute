import {Log} from "./Log";
import {ExtensionsManager} from "./ExtensionsManager";
import {DiscordClient} from "./DiscordClient";
import {CommandHandler} from "./CommandHandler";
import {APIManager} from "./API/APIManager";
import {EventManager} from "./EventManager";
import {PermissionsManager} from "./PermissionsManager";
import {Watchdg} from "../watchdog/Watchdg";

export class DisputeBot {
    static Logger:Log;
    static ExMan:ExtensionsManager;
    static DiscordClient: DiscordClient;
    static CommandHandler:CommandHandler;
    static EventMan:EventManager;
    static API:APIManager;
    static config = [];
    static permissions = [];
    static Watchdog:Watchdg;

    private static debugMode = false;

    constructor(watchdog:Watchdg = null){
        DisputeBot.Logger = new Log("CORE");
        DisputeBot.Logger.info("Dispute v1.0.0");
        DisputeBot.Logger.info("  Build 1234");
        DisputeBot.Logger.info("  Developed by Terra\n");
        if(watchdog != null)
            DisputeBot.hookWatchdg(watchdog);
        DisputeBot.Logger.debug("Initializing EventMan...");
        DisputeBot.EventMan = new EventManager();
        DisputeBot.Logger.debug("Initializing ExMan...");
        DisputeBot.ExMan = new ExtensionsManager();
        DisputeBot.config = DisputeBot.ExMan.ConfMan.getConfFor('core');
        DisputeBot.permissions = DisputeBot.ExMan.ConfMan.getConfFor('permissions');
        if(DisputeBot.config['debug'] == true) {
            DisputeBot.debugMode = true;
            DisputeBot.Logger.warning("\n ! ! !  W A R N I N G ! ! ! " +
                "\n DEBUG MODE ENABLED - DISABLE IF NOT REQUIRED FOR DEVELOPMENT!\n");
        }
        DisputeBot.Logger.info("Preparing Discord interface...");
        DisputeBot.DiscordClient = new DiscordClient();
        DisputeBot.CommandHandler = new CommandHandler(new PermissionsManager());
        DisputeBot.Logger.info("Initializing API");
        DisputeBot.API = new APIManager();
        DisputeBot.API.registerHandlers();
        DisputeBot.Logger.info("Starting API...");
        DisputeBot.API.startServer();

        if(DisputeBot.config['token'] != undefined && DisputeBot.config['token'] != "") {
            DisputeBot.start();
        } else {
            if(DisputeBot.API.isWebUIAvailable()) {
                DisputeBot.Logger.warning("\n\n   Dispute has not been set up yet (Bot token missing)!\n\n   Please visit http://localhost:8081/#/setup for initial configuration\n");
                DisputeBot.Logger.info("Waiting for web-setup to complete...");
            } else {
                DisputeBot.Logger.error("\n\n   Dispute has not been set up yet (Bot token missing)! Cannot continue.");
                process.exit(1);
            }
        }
    }

    static debugModeEnabled(){
        return DisputeBot.debugMode;
    }

    static start(token?:string){
        DisputeBot.Logger.info("Loading core...");
        DisputeBot.ExMan.loadCoreModule();
        DisputeBot.Logger.info("Preparing extensions...");
        DisputeBot.ExMan.loadExtensions();
        if(DisputeBot.config['debug'] == true) {
            DisputeBot.Logger.warning("\n ! ! !  W A R N I N G ! ! ! " +
                "\n DEBUG MODE ENABLED - DISABLE IF NOT REQUIRED FOR DEVELOPMENT!\n");
            DisputeBot.Logger.info("Loading debug...");
            DisputeBot.ExMan.loadDebugModule();
        }
        DisputeBot.Logger.info("Connecting to Discord...");
        if(token != undefined)
            DisputeBot.DiscordClient.connect(token);
        else
            DisputeBot.DiscordClient.connect(DisputeBot.config['token']);

    }

    static hookWatchdg(watchdog:Watchdg){
        DisputeBot.Logger.info("Watchdg hooked!");
        this.Watchdog = watchdog;
    }
}