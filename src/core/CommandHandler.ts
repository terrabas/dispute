import {ArgCommand} from "./Models/ArgCommand";
import {DisputeBot} from "./DisputeBot";
import {BaseCommand} from "./Models/BaseCommand";
import * as extract from 'extract-values';
import {CommandType} from "./Models/CommandType";
import {Argument} from "./Models/Argument";
import {Log} from "./Log";
import {ArgumentList} from "./Models/ArgumentList";
import {PatternCommand} from "./Models/PatternCommand";
import {PermissionsManager} from "./PermissionsManager";
import {DiscordClient} from "./DiscordClient";

export class CommandHandler {
    private commands: BaseCommand[] = [];
    private Logger: Log;
    private PermMan: PermissionsManager;

    constructor(permMan: PermissionsManager) {
        this.PermMan = permMan;
        DiscordClient.getClient().on("message", this.handleMessage.bind(this));
        this.Logger = new Log("Command");
    }

    getCommandList(user) {
        let commandList = [];
        this.commands.forEach(command => {
            if (this.PermMan.checkPerm(user, command)) {
                commandList.push({
                    category: command.category,
                    pattern: command.pattern,
                    description: command.description,
                });
            }
        });
        return commandList;
    }

    public getPermMan() {
        return this.PermMan;
    }

    private async handleMessage(message) {
        try {
            //this.Logger.debug("Received message event");
            if (message.author.bot || message.guild == null)
                return;
            if (!message.content.startsWith(DisputeBot.config['prefix']))
                return;

            if (DisputeBot.config['listenIn'][0] != "*") {
                if (DisputeBot.config['listenIn'].find(id => id == message.channel.id) == undefined)
                    return;
            }

            //this.Logger.debug("Command received. Processing...");
            let input = this.dispatcher.parse(message);

            if (input == null && !DisputeBot.config['ignoreUnknownCommand'])
                return message.channel.send("`Did you mean me? If so then you're missing the command`");
            else if (input == null && DisputeBot.config['ignoreUnknownCommand'])
                return;

            let command = this.findCommand(input.toLowerCase());
            if (command == null && !DisputeBot.config['ignoreUnknownCommand'])
                return message.channel.send("`Sorry, but I don't know that command`");
            else if (command == null && DisputeBot.config['ignoreUnknownCommand'])
                return;

            if (!this.PermMan.checkPerm(message.author, command))
                return message.channel.send("`You don't have the required amount of fluff to do that! (perm denied)`");


            // If argument based, fill arguments with values and check if required args are missing
            if (command.type == CommandType.ARGUMENT_BASED) {
                let argCommand = command as ArgCommand; // Cast to ArgCommand
                let argList: Argument[] = [];
                let args = "";
                //let values = extract(input, "{command} {args}");
                let values = {
                    command: input.split(' ')[0],
                    args: input.substr(input.indexOf(' ') + 1)
                };
                // If someone includes a "?" as argument, we want to see the help
                if (values != null && values.args != null) {
                    if (values.args.trim() == "?") {
                        // Send help for given command
                        message.channel.send(argCommand.getHelp());
                        return;
                    }
                }
                // Check required arguments
                let stop = false;
                argCommand.getArguments().forEach(arg => {
                    if (arg.required && values == null) {
                        message.channel.send("Argument '" + arg.name + "' required!");
                        stop = true;
                        return;
                    }
                });
                if (stop)
                    return;
                // If command has args, check them
                if (values != null && argCommand.args.length >= 1) {
                    this.Logger.debug("BEP BEP: " + values.args);
                    let extractor = "";
                    let extractors = [];
                    let i = 0;
                    let givenArgs = values.args.split(" ");
                    // TODO: Detect too many arguments
                    argCommand.getArguments().forEach(arg => {
                        if (givenArgs[i] != undefined && givenArgs[i].includes("\"")) {
                            extractor = extractor + "\"{" + arg.name + "}\" ";
                        } else
                            extractor = extractor + "{" + arg.name + "} ";
                        extractors.push(arg.name);
                        i++;
                    });
                    extractor = extractor.trim();
                    args = extract(values.args, extractor);
                    if (args == null) {
                        // Input is shorter than extractor would expect. Do manual conversion
                        if (extractor.split(' ').length > values.args.split(' ').length) {
                            let argTemp = values.args.split(' ');
                            this.Logger.debug(argTemp.length);
                            // @ts-ignore
                            args = [];
                            for (let y = 0; y < argTemp.length; y++) {
                                this.Logger.debug("BEEP");
                                this.Logger.debug(extractors[y]);
                                // @ts-ignore
                                args[extractors[y]] = argTemp[y];
                            }
                        }
                    }
                    this.Logger.debug(JSON.stringify(args));
                    this.Logger.debug(extractor);
                    this.Logger.debug(values.args);
                    // assign values
                    argCommand.getArguments().forEach(arg => {
                        if (args[arg.name] == undefined) {
                            if (arg.required) {
                                message.channel.send("Argument '" + arg.name + "' is required!");
                                stop = true;
                            }
                        } else {
                            arg.value = args[arg.name];
                            // push into arg list
                            argList[arg.name] = arg;
                            argList.push(arg);
                        }
                    });
                }
                if (stop)
                    return;
                this.Logger.debug("Executing command '" + argCommand.category + "', registered by extension '" + argCommand.fromExtension + "' with arguments: " + JSON.stringify(args));
                //this.Logger.debug(JSON.stringify(argList));
                message.channel.startTyping();
                // @ts-ignore
                command.execute(message, new ArgumentList(argList));
                message.channel.stopTyping();
                return;
            } else {
                // Command is pattern based
                let patCommand = command as PatternCommand; // Cast to PatternCommand
                // Find correct pattern
                let argList = [];
                let values = null;
                let _pattern = "";
                patCommand.pattern.forEach(pattern => {
                    values = extract(input, pattern);
                    if (values != null) {
                        // Correct pattern found. Generate args
                        _pattern = pattern;
                        for (let arg in values) {
                            //this.Logger.debug("arg: "+arg);
                            //this.Logger.debug("values: "+JSON.stringify(values));
                            let ArgObj = new Argument(true, arg);
                            ArgObj.setVal(values[arg]);
                            //this.Logger.debug(JSON.stringify(ArgObj));
                            argList.push(ArgObj);
                        }
                        return;
                    }
                });
                this.Logger.debug("Executing pattern command '" + _pattern + "', registered by extension '" + patCommand.fromExtension + "' with arguments: " + JSON.stringify(argList));
                message.channel.startTyping();
                // @ts-ignore
                command.execute(message, new ArgumentList(argList));
                message.channel.stopTyping();
                return;
            }
        } catch (e) {
            this.Logger.error("An error occurred: " + e.message);
            this.Logger.error(e);
            message.channel.send("UwU\nSowwy, I did an oopsie\n\n`An error occurred`");
            message.channel.stopTyping();
            return;
        }
    }

    private findCommand(input: string): BaseCommand {
        this.Logger.debug("Resolving command: " + input);
        let values = {
            command: input.split(' ')[0],
            args: input.substr(input.indexOf(' ') + 1)
        };
        if (values.command == undefined)
            return null;
        let retCommand = null;
        this.commands.forEach(command => {
            //this.Logger.debug("Testing command...");
            if (command.type == CommandType.ARGUMENT_BASED) {
                //this.Logger.debug("'"+command.category+"','"+input.trim()+"', values:"+JSON.stringify(values));
                if ((values != null && command.category == values.command) || (values == null && input.trim() == command.category)) {
                    //this.Logger.debug("Command found");
                    retCommand = command;
                    return;
                }
            }
            if (command.type == CommandType.PATTERN_BASED) {
                command.pattern.forEach(pattern => {
                    // this.Logger.debug("'"+pattern+"', input:'"+input.trim()+"', values:"+JSON.stringify(extract(input, pattern)));
                    if (extract(input, pattern) != null) {
                        retCommand = command;
                        return;
                    }
                });
            }
        });
        return retCommand;
    }

    private dispatcher = {

        parse: function (message) {

            let result = message.content
                .slice(DisputeBot.config['prefix'].length)
                .trim();

            if (!result.length) {
                return null;
            }
            return result;
        },

    };

    // Adds a command
    add(command: BaseCommand, extensionName: string) {
        // Check for duplicates
        let stop = false;
        if (command.type == CommandType.ARGUMENT_BASED) {
            let argCommand = command as ArgCommand;
            this.commands.forEach(_command => {
                if (_command.type == CommandType.ARGUMENT_BASED) {
                    if (_command.category == argCommand.category) {
                        this.Logger.error("Trying to register command '" + argCommand.category + "', but it has already been registered previously. Skipping...");
                        stop = true;
                        return;
                    }
                }
                // TODO: Corss-check with pattern based
            });
            if (stop)
                return;
            // Register command
            argCommand.fromExtension = extensionName;
            this.commands.push(argCommand);
            this.Logger.debug("Command '" + argCommand.category + "' has been registered!");
        } else {
            // Pattern based
            let pattCommand = command as PatternCommand;
            this.commands.forEach(_command => {
                if (_command.type == CommandType.PATTERN_BASED) {
                    let _patCommand = _command as PatternCommand;
                    for (let pattern of _patCommand.pattern) {
                        for (let secPattern of pattCommand.pattern) {
                            if (pattern == secPattern) {
                                this.Logger.error("Trying to register pattern '" + pattern + "', but it has already been registered previously. Skipping...");
                                stop = true;
                                return;
                            }
                        }
                    }
                }
                // TODO: Cross-check with arg based
            });
            if (stop)
                return;
            // Register command
            pattCommand.fromExtension = extensionName;
            this.commands.push(pattCommand);
            this.Logger.debug("Pattern commands '" + JSON.stringify(pattCommand.pattern) + "' have been registered!");
        }
    }
}