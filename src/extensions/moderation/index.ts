import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {Moderation} from "./core/Moderation";

export class moderation extends Extension {

    setup(context: InstallContext) {
        super.setup(context);
        new Moderation(this).setup(context);
    }
}


