export class ModEntry {
    public timestamp:any;
    public type:string = "undefined";
    public until:any = null;
    public issuedBy:string = null;
    public issuedByID:number = null;
    public issuedFor:string = null;
    public reason:string = "No reason given";

    constructor(){
        this.timestamp = Date.now();
    }

}