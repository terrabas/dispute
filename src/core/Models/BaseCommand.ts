import {CommandType} from "./CommandType";
import {Argument} from "./Argument";
import {ArgumentList} from "./ArgumentList";

export class BaseCommand {
    category:string = null;
    pattern:any = [];
    fromExtension:string = "";
    defaultAllow = false;

    constructor(public type:CommandType, public description?:string, public permission:string = null){

    }

    getHelp(){
        return "No help available";
    }

    // Arguments are passed like ["arg1" => Argument (Object) ]
    // Only arguments with values are passed!
    execute(message, args:ArgumentList){
        message.channel.send("Not implemented");
        return;
    }
}