import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {PatternCommand} from "../../core/Models/PatternCommand";
import {ArgumentList} from "../../core/Models/ArgumentList";
import {Player} from "./player";
import {Item} from "./item";

let players = {};

export class streamer extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new joinVoice());
        context.command.add(new leaveVoice());
        context.command.add(new addMedia());
        context.command.add(new listMedia());
        context.command.add(new skipMedia());
    }
}

export class joinVoice extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "join my voice channel",
            "join"
        ]);
    }

    execute(message, args: ArgumentList) {
        let channel = message.member.voiceChannel.join().then(connection => {
            let player = new Player(connection);
            players[message.member.guild] = player;
            players[message.member.guild].start();

            message.channel.send("> I've joined your voice channel.");
        });

    }
}

export class leaveVoice extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "leave the voice channel",
            "leave"
        ]);
    }

    execute(message, args: ArgumentList) {
        players[message.member.guild].disconnect();
        players[message.member.guild] = null;

        message.channel.send("> I've left the voice channel.");
    }
}

export class addMedia extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "add the media {url}",
            "add {url}"
        ]);
    }

    execute(message, args: ArgumentList) {
        if (!Item.valid(args.getArgument('url').get())) {
            return message.channel.send("> I can't play that type of media format.");
        }

        let item = new Item(args.getArgument('url').get());

        players[message.member.guild].queue(item);
        players[message.member.guild].start();

        message.channel.send("> I've added that media to the queue.");
    }
}

export class skipMedia extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "skip the current media",
            "skip"
        ]);
    }

    execute(message, args: ArgumentList) {
        players[message.member.guild].stop();

        message.channel.send("I've skipped the current media.");
    }
}

export class listMedia extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "list the playlist",
            "list"
        ]);
    }

    execute(message, args: ArgumentList) {
        message.channel.send("I've currently got " + players[message.member.guild].playlist.length + " media items in the queue.");
    }
}