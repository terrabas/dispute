import {EventManager} from "../EventManager";

export class TimedEvent {

    constructor(public onEventFunctionName:string, public onTime:number, public parms:any, public extension:string, public repeat:boolean = false, public repeatTime:number = null) {

    }

    public remove(){
        EventManager.removeTimedEvent(this);
    }

    public serialize(){
        return {func:this.onEventFunctionName, time:this.onTime, parm:this.parms,ext:this.extension,rpt:this.repeat,rptt:this.repeatTime};
    }

    public static deserialize(timedEventObj:Object){
        return new TimedEvent(timedEventObj['func'], timedEventObj['time'], timedEventObj['parm'], timedEventObj['ext'], timedEventObj['rpt'], timedEventObj['rptt']);
    }

}
