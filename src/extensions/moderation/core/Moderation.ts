import {InstallContext} from "../../../core/Models/InstallContext";
import {Extension} from "../../../core/Extension";
import {Log} from "../../../core/Log";
import {DataFile, Encoding} from "../../../core/Storage/DataFile";
import {ModDatabase} from "./ModDatabase";
import {WarnMember} from "../commands/WarnMember";
import {MemberInfo} from "../commands/MemberInfo";
import {Event} from "../../../core/Models/Event";
import {ExportDB} from "../commands/ExportDB";
import {SilentWarn} from "../commands/SilentWarn";
import {KickMember} from "../commands/KickMember";
import {BanMember} from "../commands/BanMember";
import {MainPage} from "../UI/MainPage";

export class Moderation {
    Logger:Log;

    constructor(public extension: Extension) {
        this.Logger = extension.Logger;
    }


    public setup(context: InstallContext){
        this.Logger.info(" -----  MODERATION EXTENSION ----- ");
        this.Logger.info(" - Loading moderation database 'userData'...");
        ModDatabase.userData = context.storage.requestDataFile('userData', Encoding.JSON);
        this.Logger.info(" - Registering commands...");
        context.command.add(new WarnMember());
        context.command.add(new MemberInfo());
        context.command.add(new SilentWarn());
        context.command.add(new KickMember());
        context.command.add(new BanMember());
        context.command.add(new ExportDB());
        this.Logger.info(" - Registering events...");
        context.event.registerDisputeEvent(new Event('shutdown', ModDatabase.onShutdown));
        this.Logger.info(" - Registering UI...");
        context.webui.addMainPage(new MainPage());
        this.Logger.info(" ----- M O D T O O L S    R E A D Y ------");
    }
}