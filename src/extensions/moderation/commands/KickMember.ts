import {ArgCommand} from "../../../core/Models/ArgCommand";
import {Argument} from "../../../core/Models/Argument";
import {ArgumentList} from "../../../core/Models/ArgumentList";
import {Helpers} from "../core/Helpers";
import {ModDatabase} from "../core/ModDatabase";

export class KickMember extends ArgCommand {

    constructor() {
        super("kick", [new Argument(true, "username", "The current server username of the user"), new Argument(true, "reason", "The reason for the kick")], "Kicks a user with given reason and logs it. Also suggest further action if required.");
    }

    execute(message, args: ArgumentList) {

        let result = Helpers.getUsers(message, args.getArgument("username").get());

        if (result.length >= 2)
            return message.channel.send("`Multiple members with username found. This is unexpected.`");

        if (result.length == 0)
            return message.channel.send("`No user found!`");

        if (args.getArgument("reason").get() == "") {
            return message.channel.send("`Invalid reason given: Can't be empty!`");
        }

        ModDatabase.addKick(result[0], message.author, args.getArgument("reason").get());

        result[0].send("Beep-boop\n" +
            "Good day!\n\n" +
            "I'm an automated message to inform you that you were kicked by a member of staff from the server " + message.guild.name + " with the following reason:\n\n" +
            "`" + args.getArgument("reason").get() + "`\n\n" +
            "In case you think that this kick was not justified or you wish to appeal it, please contact a member of staff.\n" +
            "Do not reply to this message.");

        result[0].kick(args.getArgument("reason").get()).catch(function (error) {
            message.channel.send("`ERROR: Kick could not be executed! Please do it manually. The database entry is unaffected by this.`");
        });

        message.channel.send("`The kick has been successfully logged in the database, executed and the member has been notified.`");

        let infringements = ModDatabase.getModEntries(result[0]).length;
        if(infringements > 3){
            message.channel.send("`User now has "+infringements+" rule infringements!`");
            if(infringements <= 3) {
                message.channel.send("`Suggested action: Kick`");
            } else if (infringements <= 5){
                message.channel.send("`Suggested action: Temporary ban (24h)`");
            } else if (infringements > 5) {
                message.channel.send("`Suggested action: Permanent ban`");
            }
        }
    }
}