import {type} from "os";
import {SIGINT} from "constants";

export class Watchdg {
    static onRestartFunc;
    watchFnc:any;
    runFnc:any;
    conf:[];
    firstStart:boolean = true;

    static shutdownComplete:boolean = false;

    restartCount:number = 0;
    lastStartTimestamp:Number = null;

    requestedRestart:boolean = false;
    currentStartByRestartRequest:boolean = false;

    constructor(func:any, conf:[] = []) {
        this.watchFnc = func;
        this.conf = conf;
    }

    static onRestart(func:any){
        Watchdg.onRestartFunc = func;
    }

    static sendShutdownComplete(){
        Watchdg.shutdownComplete = true;
    }

    restart(){
        this.requestedRestart = true;
        console.info("Watchdg: Restart requested! Waiting for function to end...");
        Watchdg.onRestartFunc();
        //process.kill(process.pid, "SIGINT");
    }

    restartedByWatchdg(){
        return this.currentStartByRestartRequest;
    }

    execute(){
        try {
            this.runFnc = this.watchFnc;
            if(this.firstStart) {
                console.info("Watchdg: This process is protected by Watchdg!");
                console.info("Watchdg:    Version 1.0 (c) Terra");
                console.info("\n");
                this.firstStart = false;
            } else {
                delete this.runFnc;
            }
            this.lastStartTimestamp = Date.now();

            this.runFnc(this);
            if(this.requestedRestart){
                this.requestedRestart = false;
                this.currentStartByRestartRequest = true;
                this.execute();
            }

        } catch (e) {
            console.warn("\nWatchdg: Caught unexpected end of function!");
            console.warn("Watchdg: Err: "+e);

            let currTime = Date.now();

            var restartLimit = this.conf['restartLimit'];
            var healingThreshold = this.conf['healingThreshold'];

            if(restartLimit == null) {
                restartLimit = 10;
            }

            if(healingThreshold == null) {
                healingThreshold = 1000; // 1 second
            }

            if(this.lastStartTimestamp + healingThreshold > currTime) {
                console.warn("\nWatchdg: Time too short between crashes: Assuming no self-healing!");
                console.warn("Watchdg: Disabled!");

                return;
            }

            if(restartLimit >= this.restartCount) {
                console.warn("\nWatchdg: Restarting function: Attempt "+this.restartCount+" of "+restartLimit);
                this.restartCount++;
                this.currentStartByRestartRequest = false;
                this.execute();
            }

        }
    }
}