import {ModEntry} from "./ModEntry";

export class Kick extends ModEntry {

    constructor(kickedUser:any, byUser:any, reason:string) {
        super();
        this.type = "kick";
        this.issuedBy = byUser.username;
        this.issuedByID = byUser.id;
        this.issuedFor = kickedUser.username;
        this.reason = reason;
    }
}