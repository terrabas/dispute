import {ArgCommand} from "../../../core/Models/ArgCommand";
import {Argument} from "../../../core/Models/Argument";
import {ArgumentList} from "../../../core/Models/ArgumentList";
import {Helpers} from "../core/Helpers";
import {ModDatabase} from "../core/ModDatabase";
import {EventManager} from "../../../core/EventManager";
import {TimedEvent} from "../../../core/Models/TimedEvent";

export class BanMember extends ArgCommand {

    constructor() {
        super("ban", [new Argument(true, "username", "The current server username of the user"), new Argument(true, "reason", "The reason for the ban")], "Bans a user with given reason and logs it.");
    }

    execute(message, args: ArgumentList) {

        let result = Helpers.getUsers(message, args.getArgument("username").get());

        if (result.length >= 2)
            return message.channel.send("`Multiple members with username found. This is unexpected.`");

        if (result.length == 0)
            return message.channel.send("`No user found!`");

        if (args.getArgument("reason").get() == "") {
            return message.channel.send("`Invalid reason given: Can't be empty!`");
        }

        ModDatabase.addBan(result[0], message.author, args.getArgument("reason").get());

        result[0].send("Beep-boop\n" +
            "Good day!\n\n" +
            "I'm an automated message to inform you that you were banned by a member of staff from the server " + message.guild.name + " with the following reason:\n\n" +
            "`" + args.getArgument("reason").get() + "`\n\n" +
            "In case you think that this ban was not justified or you wish to appeal it, please contact a member of staff.\n" +
            "Do not reply to this message.");

        result[0].ban(args.getArgument("reason").get()).catch(function (error) {
            console.error(error);
            message.channel.send("`ERROR: Ban could not be executed! Please do it manually. The database entry is unaffected by this.`");
        });

        message.channel.send("`The ban has been successfully logged in the database, executed and the member has been notified.`");

        let infringements = ModDatabase.getModEntries(result[0]).length;
        if(infringements > 3){
            message.channel.send("`User now has "+infringements+" rule infringements!`");
        }
    }
}

export class BanFor extends ArgCommand {

    constructor() {
        super("banfor", [new Argument(true, "username", "The current server username of the user"), new Argument(true, "time","Time in days until ban is lifted"), new Argument(true, "reason", "The reason for the ban")], "Bans a user with given reason and logs it.");
    }

    execute(message, args: ArgumentList) {

        let result = Helpers.getUsers(message, args.getArgument("username").get());

        if (result.length >= 2)
            return message.channel.send("`Multiple members with username found. This is unexpected.`");

        if (result.length == 0)
            return message.channel.send("`No user found!`");

        if (args.getArgument("reason").get() == "") {
            return message.channel.send("`Invalid reason given: Can't be empty!`");
        }

        if(!Helpers.isNumber(args.getArgument('time').get())){
            return message.channel.send("`Invalid time!`");
        }

        ModDatabase.addBan(result[0], message.author, args.getArgument("reason").get());

        result[0].send("Beep-boop\n" +
            "Good day!\n\n" +
            "I'm an automated message to inform you that you were banned by a member of staff from the server " + message.guild.name + " for "+args.getArgument('time').get()+" day(s) with the following reason:\n\n" +
            "`" + args.getArgument("reason").get() + "`\n\n" +
            "In case you think that this ban was not justified or you wish to appeal it, please contact a member of staff.\n" +
            "Do not reply to this message.");

        result[0].ban(args.getArgument("reason").get()).catch(function (error) {
            console.error(error);
            message.channel.send("`ERROR: Ban could not be executed! Please do it manually. The database entry is unaffected by this.`");
        });

        let parms = {};
        parms['guildID'] = message.guild.id;
        parms['userID'] = result[0].id;

        EventManager.addTimedEvent(new TimedEvent('modtools_unban', (Number(args.getArgument('time').get())*86400000), parms, 'moderation'));

        message.channel.send("`The ban has been successfully logged in the database, executed and the member has been notified.`");
        message.channel.send("`The member will be un-banned in "+args.getArgument('time').get()+" day(s)!`");

        let infringements = ModDatabase.getModEntries(result[0]).length;
        if(infringements > 3){
            message.channel.send("`User now has "+infringements+" rule infringements!`");
        }
    }
}