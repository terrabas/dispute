import {Log} from "../Log";
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import {ConfigManager} from "../ConfigManager";
import {DisputeBot} from "../DisputeBot";
import {EventManager} from "../EventManager";
import {MinimalEvent} from "../Models/MinimalEvent";
import {DiscordClient} from "../DiscordClient";
import * as FS from "fs";
import {UIManager} from "./UIManager";

export class APIManager {
    private Logger: Log;
    private app;
    private ui: UIManager;

    private webuiAvailable: boolean = false;

    constructor() {
        this.Logger = new Log("API");
        this.app = express();
        this.app.use(cors());
        this.app.use(bodyParser.json());
        EventManager.addDisputeEvent(new MinimalEvent("shutdown", () => {
            this.Logger.info("Stopping API...");
            this.app.server.close()
        }))
    }

    public getUIManager() {
        return this.ui;
    }

    registerHandlers() {
        if (FS.existsSync(ConfigManager.getRootPath() + "/webui")) {
            this.app.use(express.static(ConfigManager.getRootPath() + "/webui"));
            this.webuiAvailable = true;
            this.Logger.info("webUI OK!");
            this.ui = new UIManager(this);
        } else {
            this.Logger.error("webUI installation NOT FOUND! The integrated webUI will not be available.");
            this.Logger.warning("Please install the webUI to fix this error")
        }
        this.app.post('/setup/keys', (req, res) => {
            this.Logger.debug(req.body);
            if (req.body.token == undefined)
                return res.send({success: false});
            DisputeBot.start(req.body.token);
            return res.send({success: true});
        });
        this.app.get('/status', (req, res) => res.send({
            success: true,
            status: DiscordClient.getClient().status,
        }));
        this.app.get('/userlist', (req, res) => {
            let members = [];
            DiscordClient.getClient().guilds.forEach(guild => {
                guild.members.forEach(member => {
                    members.push({
                        id: member.user.id,
                        username: member.user.username,
                        avatarUrl: member.user.avatarURL
                    });
                })
            });
            return res.send({
                success: true,
                memberList: members,
            })
        });
        this.app.get('/extensions', (req, res) => {
            let activeExtensions = DisputeBot.ExMan.getExtensions();
            let disabledExtensions = DisputeBot.ExMan.getDisabledExtensions();
            return res.send({
                success: true,
                data: {enabled: activeExtensions, disabled: disabledExtensions},
            })
        });
        this.app.get('/uinavbar', (req, res) => {
            return res.send({
                success: true,
                data: this.getUIManager().getMainPageTitles(),
            })
        });
        this.app.get('/log', (req, res) => {
            let members = [];
            return res.send({
                success: true,
                log: Log.buffer,
            })
        });

    }

    registerExtAPICall(path: string, onCall: (body) => any) {
        this.app.get('/ext/' + path, (req, res) => {
            onCall(req.body).then((data) => {
                res.send({
                    success: true,
                    data: data,
                })
            }).catch((error) => {
                res.status(500, {
                    success: false
                });
            });
        });
    }

    startServer() {
        this.app.server = this.app.listen(8081, () => {
            this.Logger.info("API ready on port 8081");
        });
    }

    isWebUIAvailable(): boolean {
        return this.webuiAvailable;
    }

}