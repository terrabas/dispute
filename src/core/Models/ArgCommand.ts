import {Argument} from "./Argument";
import {BaseCommand} from "./BaseCommand";
import {CommandType} from "./CommandType";

export class ArgCommand extends BaseCommand {
    constructor(public category:string, public args:Argument[], public description?:string, public permission:string = null){
        super(CommandType.ARGUMENT_BASED);
    }

    getArguments():Argument[] {
        return this.args;
    }

    getHelp():string {
        if(this.description == undefined)
            return "No help available for this command";
        let help = "**Command help**\n> "+this.category+": "+this.description+"\n\n";
        // Build command example
        let argLine = "Syntax: `"+this.category+" ";
        if(this.args.length >= 1) {
            let argPos = 1;
            this.args.forEach(arg => {
                if (arg.name == undefined) {
                    if (arg.required)
                        argLine = argLine + "arg" + argPos + " ";
                    else
                        argLine = argLine + "[arg" + argPos + "] ";
                } else {
                    if (arg.required)
                        argLine = argLine + arg.name + " ";
                    else
                        argLine = argLine + "[" + arg.name + "] ";
                }
                argPos++;
            });
            // Reset for next loop
            argPos = 1;
            // Format as code line
            argLine = argLine + "`\n\n";
            help = help + argLine;
            // Build argument info
            this.args.forEach(arg => {
                let commandInfoLine = "";
                if (arg.name == undefined) {
                    if (arg.required)
                        commandInfoLine = "arg" + argPos + ": ";
                    else
                        commandInfoLine = "[arg" + argPos + "]: ";
                } else {
                    if (arg.required)
                        commandInfoLine = arg.name + ": ";
                    else
                        commandInfoLine = "[" + arg.name + "]: ";
                }
                if (arg.description == undefined)
                    commandInfoLine = commandInfoLine + " --- \n";
                else
                    commandInfoLine = commandInfoLine + arg.description + "\n";
                help = help + commandInfoLine;
            });
        } else {
            help = help + argLine + "`\n\nThis command has no arguments";
        }
        return help;
    }
}