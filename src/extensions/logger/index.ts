import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {Event} from "../../core/Models/Event";
import * as delay from 'await-delay';
import {Log} from "../../core/Log";
import {TimedEvent} from "../../core/Models/TimedEvent";
import {EventManager} from "../../core/EventManager";
import {TimedUserEvent} from "../../core/Models/TimedUserEvent";
import {DisputeBot} from "../../core/DisputeBot";
import {DiscordClient} from "../../core/DiscordClient";

export class logger extends Extension {
    logChannel;

    setup(context: InstallContext) {
        super.setup(context);
        if(this.Config["logChannelID"] == null || this.Config["logChannelID"] == ""){
            this.Logger.error("No logChannelID defined in the logger config! Extension disabled");
            return;
        }

        this.logChannel = DiscordClient.getClient().channels.get(this.Config["logChannelID"]);
    }

}

export class JoinEvent extends Event {
    constructor(public Config, public logger:Log){
        super('guildMemberAdd', (member) => {
            logger.debug("Guild member '"+member.id+"' joined guild '"+member.guild.id+"'");
            if (member.bot || member.guild == null || member.guild.id in Config['guilds'] == false) {
                return;
            }

            var message = Config['guilds'][member.guild.id].message;

            /*for (let name in transforms) {
                message = message.replace(`{${name}}`, transforms[name](member));
            } */

            delay(Config['delay']);
            member.guild.channels.get(Config['guilds'][member.guild.id].channel).send(message);

            let event = new TimedUserEvent(member.id,"ukf_notifyunassigned",Date.now()+10000, null, "introduction");
            EventManager.addTimedEvent(event);

        })
    }
}

