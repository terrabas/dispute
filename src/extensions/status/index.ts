import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
const random = require('random-item');
import {Event} from "../../core/Models/Event";

export class status extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.event.registerDiscordEvent(new ReadyEvent(this.Config));
    }
}

export class ReadyEvent extends Event {
    constructor(public Config){
        super('ready', () => {
            let update = function() {
                let category = random(Object.keys(Config['statuses']));
                let activity = random(Config['statuses'][category]);

                this.client.user.setPresence({
                    status: "online",
                    game: {
                        name: activity,
                        type: category.toUpperCase()
                    }
                });
            }.bind(this);
            this.client.user.setStatus("online");
            update();

            let interval = this.client.setInterval(update, Config['frequency']);

        })
    }
}