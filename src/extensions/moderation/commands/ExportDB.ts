import {ArgCommand} from "../../../core/Models/ArgCommand";
import {ArgumentList} from "../../../core/Models/ArgumentList";
import {ModDatabase} from "../core/ModDatabase";

export class ExportDB extends ArgCommand {
    constructor() {
        super("modexport",[],"Exports the moderation database");
    }

    execute(message, args: ArgumentList) {
        message.channel.send("> Exporting moderation database...\n> This might take a long time");

        ModDatabase.exportToCSV();
        message.channel.send("> Export complete", { files: ["./tmp/modUserDBexport.csv"] });

        return;
    }
}