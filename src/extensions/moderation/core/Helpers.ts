export class Helpers {
    static getUsers(message:any, usernameOrID:any):any[]{
        let result = [];
        let isnum = /^\d+$/.test(usernameOrID);
        // Look for member
        message.guild.members.cache.forEach(member => {
            if (!isnum) {
                if (member.displayName.startsWith(usernameOrID))
                    result.push(member);
            } else {
                if (member.id == usernameOrID)
                    result.push(member);
            }
        });
        return result;
    }
    static isNumber(value: string | number): boolean
    {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    }
}