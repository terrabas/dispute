export class Argument {
    value:string = null;
    constructor(public required:boolean, public name:string, public description?:string){

    }

    setVal(value:string){
        this.value = value;
    }

    get():string{
        return this.value;
    }
}