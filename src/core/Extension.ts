import {Log} from "./Log";
import {InstallContext} from "./Models/InstallContext";

export class Extension {

    constructor(public Config:[], public Logger:Log){

    }

    setup(context:InstallContext){

    }
}