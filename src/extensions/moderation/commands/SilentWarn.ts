import {ArgCommand} from "../../../core/Models/ArgCommand";
import {Argument} from "../../../core/Models/Argument";
import {ArgumentList} from "../../../core/Models/ArgumentList";
import {Helpers} from "../core/Helpers";
import {ModDatabase} from "../core/ModDatabase";

export class SilentWarn extends ArgCommand {

    constructor() {
        super("silentwarn", [new Argument(true, "username", "The current server username of the user"), new Argument(true, "reason", "The reason for the warning")], "Logs a user warning without notifying the user. Also suggest further action if required.");
    }

    execute(message, args: ArgumentList) {

        let result = Helpers.getUsers(message, args.getArgument("username").get());

        if (result.length >= 2)
            return message.channel.send("`Multiple members with username found. This is unexpected.`");

        if (result.length == 0)
            return message.channel.send("`No user found!`");

        if (args.getArgument("reason").get() == "") {
            return message.channel.send("`Invalid reason given: Can't be empty!`");
        }

        ModDatabase.addWarning(result[0], message.author, args.getArgument("reason").get());

        message.channel.send("`The warning has been successfully logged in the database.`");

        let infringements = ModDatabase.getModEntries(result[0]).length;
        if(infringements > 3){
            message.channel.send("`User now has "+infringements+" rule infringements!`");
            if(infringements <= 3) {
                message.channel.send("`Suggested action: Kick`");
            } else if (infringements <= 5){
                message.channel.send("`Suggested action: Temporary ban (24h)`");
            } else if (infringements > 5) {
                message.channel.send("`Suggested action: Permanent ban`");
            }
        }
    }
}