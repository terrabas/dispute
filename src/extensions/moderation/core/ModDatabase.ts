import {DataFile} from "../../../core/Storage/DataFile";
import {Warning} from "../models/Warning";
import {ModEntry} from "../models/ModEntry";
import {DiscordClient} from "../../../core/DiscordClient";
import * as fs from 'fs';
import {Kick} from "../models/Kick";
import {Ban} from "../models/Ban";

export class ModDatabase {
    public static userData:DataFile;

    public static hasEntry(user:any){
        // Checks if there is already an entry for user in the userdata database
        return !(this.userData.get(user.id) == undefined);
    }

    public static addWarning(warnedUser:any, issuedByUser:any, reason:string){
        // Check if user already in database and add empty entry if not
        if (!this.hasEntry(warnedUser))
            this.userData.set(warnedUser.id, []);

        this.userData.get(warnedUser.id).push(new Warning(warnedUser, issuedByUser, reason));

        // Commit data to disk
        this.userData.write();

    }

    public static addKick(warnedUser:any, issuedByUser:any, reason:string){
        // Check if user already in database and add empty entry if not
        if (!this.hasEntry(warnedUser))
            this.userData.set(warnedUser.id, []);

        this.userData.get(warnedUser.id).push(new Kick(warnedUser, issuedByUser, reason));

        // Commit data to disk
        this.userData.write();

    }

    public static addBan(warnedUser:any, issuedByUser:any, reason:string){
        // Check if user already in database and add empty entry if not
        if (!this.hasEntry(warnedUser))
            this.userData.set(warnedUser.id, []);

        this.userData.get(warnedUser.id).push(new Ban(warnedUser, issuedByUser, reason));

        // Commit data to disk
        this.userData.write();

    }

    static getUserJSON(user:any):any{
        return this.userData.get(user.id);
    }

    public static getModEntries(user:any):ModEntry[]{
        if (!this.hasEntry(user))
            return null;

        let result:ModEntry[] = [];
        this.getUserJSON(user).forEach(function (element) {
            let modEntry:ModEntry = element;
            result.push(modEntry);
        });

        return result;
    }

    public static async exportToCSV() {
        let contents = "SEP=,\n" +
            "# Discourse FRAMEWORK moderation database dump\n" +
            "# Generated: " + new Date(Date.now()).toDateString() + "\n#\n" +
            "# THIS IS CLASSIFIED DISCORD SERVER DATA - DO NOT REDISTRIBUTE OR PUBLISH UNLESS PERMITTED BY THE SERVER OWNER!\n\n";

        let ids = Object.keys(ModDatabase.userData.get());
        for (let i = 0; i < ids.length; i++) {
            let user = await DiscordClient.getClient().fetchUser(ids[i]);
            contents += "# USERDATA ID " + ids[i] + " AKA " + user.username + "\n";
            contents += "uid:" + ids[i] + "\n";
            contents += "# Timestamp, Type, Until, Issued by, Issued by ID, Issued for, reason\n";
            ModDatabase.getModEntries(user).forEach(function (modData: ModEntry) {
                contents += "\"" + modData.timestamp + "\",\"" + modData.type + "\",\"" + modData.until + "\",\"" + modData.issuedBy + "\",\"" + modData.issuedByID + "\",\"" + modData.issuedFor + "\",\"" + modData.reason + "\"\n";
            });
            contents += "\n\n";
        }

        var dir = './tmp';

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        fs.writeFileSync("./tmp/modUserDBexport.csv", contents);
        return;
    }

    public static onShutdown(){
        // Write moderation database on shutdown just to make sure
        ModDatabase.userData.write();
    }
}