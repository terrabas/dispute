FROM node:10.13.0-alpine
# Env
ENV TIME_ZONE=Europe/Vienna
ENV ENV_NAME dev
ENV EGG_SERVER_ENV dev
ENV NODE_ENV dev
ENV NODE_CONFIG_ENV dev
# Set the timezone in docker
#RUN apk --update add tzdata \\
#   && cp /usr/share/zoneinfo/Asia/Hong_Kong /etc/localtime \\
#   && echo "Europe/Vienna" > /etc/timezone \\
#   && apk del tzdata
# Create Directory for the Container
WORKDIR /usr/build/app
# Only copy the package.json file to work directory
COPY package.json .
RUN apk add --no-cache git
RUN apk add --no-cache python
RUN apk add --no-cache make
RUN apk add --no-cache gcc
RUN apk add --no-cache g++
# Install all Packages
RUN npm install
# Copy all other source code to work directory
ADD . /usr/src/app
# TypeScript
#RUN npm run tsc
# Start
CMD [ "node", "build/index.js" ]
EXPOSE 8081