import {EventHandler} from "../../../core/Models/EventHandler";
import {TimedEvent} from "../../../core/Models/TimedEvent";
import {DiscordClient} from "../../../core/DiscordClient";

export class UnbanEventHandler extends EventHandler {
    constructor() {
        super("modtools_unban");
    }

    public onEvent(event:TimedEvent):boolean{

        let guildID = event.parms['guildID'];
        let userID = event.parms['userID'];

        let guild = DiscordClient.getClient().guilds.get(guildID);

        if(guild == null){
            console.error("Unban failed: Guild not found!");
            return false;
        }

        guild.unban(userID)
            .then(user => console.log(`Unbanned ${user.username} from ${guild}`))
            .catch(console.error);
        return true;
    }

}