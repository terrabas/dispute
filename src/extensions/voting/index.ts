import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {PatternCommand} from "../../core/Models/PatternCommand";
import {ArgumentList} from "../../core/Models/ArgumentList";
import * as pluralise from "pluralize";


export class voting extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new VotingCommand(this.Config));
    }
}

export class VotingCommand extends PatternCommand {
    constructor(public config) {
        super(["start a vote of {title} with the options {options}",
            "create a poll of {title} with the options {options}",
            "vote {title} with {options}"]);
    }

    execute(message, args: ArgumentList) {
        let options = args.getArgument('options').get()
            .split(",")
            .map((current) => current.trim())
            .filter((current) => current.length);

        if (options.length < 2) {
            return message.channel.send("I can't start a vote with less than 2 options.");
        }

        if (options.length > this.config['emoji'].length - 1) {
            return message.channel.send("I can't start a vote with that many options.");
        }

        let target = message.channel.send({
            embed: {
                title: "Vote",
                description: args.getArgument('title').get(),
                fields: options.map((current, index) => ({
                    name: current,
                    value: `Vote ${this.config['emoji'][index]}`,
                    inline: true
                })),
                footer: {
                    text: "Add a reaction to cast your vote"
                }
            }
        });
        let collector;
        target.then(message => {
            collector = message.createReactionCollector((current) => this.config['emoji'].includes(current.emoji.name), {time: 60000});
            let _message = message;
            collector.on("end", function (reactions) {
                if (target.deleted) {
                    return;
                }

                _message.delete();

                let results = options.map(function (option, index) {
                    let reaction = reactions.find((reaction) => reaction.emoji.name == this.config['emoji'][index]);
                    let count = reaction != null ? reaction.count : 0;

                    return {
                        name: option,
                        value: pluralise("vote", count, true),
                        inline: true
                    };
                }.bind(this));

                message.channel.send({
                    embed: {
                        title: "Vote Results",
                        description: args.getArgument('title').get(),
                        fields: results,
                        footer: {
                            text: pluralise("total vote", reactions.reduce((value, current) => value + current.count, 0), true)
                        }
                    }
                });
            }.bind(this));
        });

    }
}