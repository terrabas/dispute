import {BaseCommand} from "./BaseCommand";
import {CommandType} from "./CommandType";

export class PatternCommand extends BaseCommand {
    constructor(public pattern:any, public description?:string, public permission:string = null){
        super(CommandType.PATTERN_BASED);
    }

    getHelp():string {
        return "No help available for pattern commands";
    }
}