//import * as BSON from "BS";
import {Log} from "./Log";
import * as fs from "fs";
import {ConfigManager} from "./ConfigManager";
import {EventManager} from "./EventManager";
import {MinimalEvent} from "./Models/MinimalEvent";
import {DataFile, Encoding} from "./Storage/DataFile";

export class StorageManager {
    static Logger:Log;
    static dataFiles:string[] = [];

    constructor(){
        StorageManager.Logger = new Log("StorMan");
        StorageManager.Logger.info("Preparing storage...");
        if(!StorageManager.checkDataDirExists()) {
            StorageManager.Logger.warning("Data folder not found! Creating...");
            fs.mkdirSync(ConfigManager.rootPath + '/data');
        }
        StorageManager.Logger.info("Checking storage integrity, please wait...");
        StorageManager.Logger.info("This might take a while");
       // StorageManager.getDataFileNames().forEach(datafile => BSON.deserialize(StorageManager.getData(datafile)));
        StorageManager.Logger.info("Check OK!");
        EventManager.addDisputeEvent(new MinimalEvent("shutdown",()=>{StorageManager.Logger.info("Writing queue...")}))

    }

    static getData(dataFileName){
        return fs.readFileSync(ConfigManager.rootPath + '/data/'+ dataFileName).toString()
    }

    public static getDataByLocation(dataFileName){
        return fs.readFileSync(ConfigManager.rootPath + '/data/'+ dataFileName+".dtf").toString()
    }

    static getDataFileNames(): string[] {
        return fs.readdirSync(ConfigManager.getRootPath() + "/data/", {withFileTypes: true})
            .filter(dirent => !dirent.isDirectory())
            .map(dirent => dirent.name)
    }

    public static requestDataFile(dataFileName, encoding:Encoding = Encoding.RAW){
        // Check if data file exists
        if(!this.checkDataFileExists(dataFileName)){
            return new DataFile(StorageManager,null,null,dataFileName, encoding);
        }
        let df = new DataFile(StorageManager,null,null,dataFileName, encoding);
        df.read();
        return df;
    }

    static checkDataFileExists(dataFileName): boolean {
        if (fs.existsSync(ConfigManager.rootPath + '/data/'+dataFileName+'.dtf'))
            return true;
        return false;
    }

    static checkDataDirExists(): boolean {
        if (fs.existsSync(ConfigManager.rootPath + '/data'))
            return true;
        return false;
    }


}