import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {ArgCommand} from "../../core/Models/ArgCommand";
import {Argument} from "../../core/Models/Argument";
import {ArgumentList} from "../../core/Models/ArgumentList";
import {Event} from "../../core/Models/Event";
import {Log} from "../../core/Log";
import {TimedEvent} from "../../core/Models/TimedEvent";
import {EventManager} from "../../core/EventManager";
import {EventHandler} from "../../core/Models/EventHandler";
import {DiscordClient} from "../../core/DiscordClient";

export class ukfurs extends Extension {

    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new makeMember(this.Config));
        context.event.registerDiscordEvent(new BoostEvent(this.Config, this.Logger));
        context.event.registerEventHandler(new NotifyUnassignedUserEventHandler())
    }
}

export class makeMember extends ArgCommand {
    constructor(public Config) {
        super("makemember",[new Argument(true,"username","The current server username of the user")],"Makes a guest stuck in the channel #introductions a member and announces it int #general");
    }

    execute(message, args: ArgumentList) {
        let guildID = this.Config['guildID'];
        let introductionID = this.Config['introductionChannelID'];
        let announceChannelID = this.Config['announceChannelID'];

        if(message.guild.id != guildID)
            return message.channel.send("`This command is not set up for this guild. Check your config`");

        // Find channel
        let channel = message.guild.channels.get(introductionID);
        if(channel == undefined)
            return message.channel.send("`Introduction channel could not be found! Check your config`");

        // Find channel
        let announceChannel = message.guild.channels.get(announceChannelID);
        if(announceChannel == undefined)
            return message.channel.send("`Announce channel could not be found! Check your config`");

        let result = [];

        let isnum = /^\d+$/.test(args.getArgument("username").get());

        // Look for member
        message.guild.members.forEach(member => {
            if(!isnum) {
                if (member.displayName.toLowerCase().startsWith(args.getArgument("username").get()))
                    result.push(member);
            } else {
                if (member.id == args.getArgument("username").get())
                    result.push(member);
            }
        });

        if(result.length >= 2)
            return message.channel.send("`Multiple members with username found. This is unexpected.`");

        if(result.length <= 0)
            return message.channel.send("`User could not be found!`");

        let role = message.guild.roles.find(role => role.name === this.Config['memberRole']);
        if(role == undefined)
            return message.channel.send("`The role configured to be set could not be found on the server. Check your config`");

        if(result[0].roles.find(role => role.name === this.Config['memberRole']) != undefined)
            return message.channel.send("`User '"+result[0].displayName+"' is already a member!`");

        result[0].addRole(role);

        announceChannel.send(":arrow_forward:  <@"+result[0].id+"> has joined the server!");

        return message.channel.send("`Successfully applied roles to user <@"+result[0].id+"> ("+result[0].displayName+") !`");
    }
}

export class BoostEvent extends Event {
    constructor(public Config, public logger:Log){
        super('nitroBoost', (member) => {
            logger.debug("Guild member '"+member.id+"' boosted guild '"+member.guild.id+"'");
            if (member.bot || member.guild == null || member.guild.id != Config['guildID']) {
                return;
            }

            member.guild.channels.get(Config['announceChannelID']).send("<@"+member.id+"> just boosted the server! Thank you very much!");
        })
    }
}

export class NotifyUnassignedUserEventHandler extends EventHandler {
    constructor() {
        super("ukf_notifyunassigned");
    }

    public onEvent(event:TimedEvent):boolean{
        // check if user has no roles assigned
        return true;
    }

}