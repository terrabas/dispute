import {Log} from "./Log";
import {Event} from "./Models/Event";
import {MinimalEvent} from "./Models/MinimalEvent";
import {DiscordClient} from "./DiscordClient";
import {Watchdg} from "../watchdog/Watchdg";
import {TimedEvent} from "./Models/TimedEvent";
import {EventHandler} from "./Models/EventHandler";
import {DataFile, Encoding} from "./Storage/DataFile";
import {StorageManager} from "./StorageManager";
import {TimedUserEvent} from "./Models/TimedUserEvent";
import {DisputeBot} from "./DisputeBot";

export class EventManager {
    static Logger: Log;
    static discordEvent: Event[] = [];
    static DisputeEvent: MinimalEvent[] = [];
    static EventHandlers: EventHandler[] = [];
    static TimedEvents: TimedEvent[] = [];

    static TimedEventsFile: DataFile;

    constructor() {
        EventManager.Logger = new Log("EventMan");

        process.on('SIGINT', () => this.onShutdown());
        Watchdg.onRestart(() => this.onShutdown());
        EventManager.Logger.debug("Fetching previously stored events...");
        EventManager.TimedEventsFile = StorageManager.requestDataFile("EventManTmdEv", Encoding.JSON);
        this.initTimedEvents();
    }

    private initTimedEvents(){
        if(EventManager.TimedEventsFile.get("timedEvents") == null){
            EventManager.Logger.debug("No timed events stored to resume!");
        } else {
            EventManager.TimedEventsFile.get("timedEvents").forEach(json => {
                EventManager.Logger.debug("LD -r E");
                EventManager.TimedEvents.push(TimedEvent.deserialize(json));
            });
        }

        EventManager.addDisputeEvent(new MinimalEvent("ready", () => {
            setInterval(this.timedEventsExecutor.bind(this),5000);
            EventManager.Logger.debug("Event loop started");
        }));

    }

    private static updateEventStorage(){
        let json = [];

        EventManager.TimedEvents.forEach(event => {
            json.push(event.serialize());
        });

        EventManager.TimedEventsFile.set("timedEvents", json);
        EventManager.TimedEventsFile.write();
    }

    private timedEventsExecutor(){
        //EventManager.Logger.debug("EventLoop fire: " + Date.now() +" | "+EventManager.TimedEvents.length);
        EventManager.TimedEvents.forEach(event => {
            if(event.onTime <= Date.now()) {
               if(!this.executeEventHandler(event)){
                   EventManager.Logger.error("Timed event "+event.onEventFunctionName+" could not be executed: EventHandler for event not found!");
               } else {
                   // TODO: clean this up
                   if (event.repeat) {
                       if (event.repeatTime == null) {
                           EventManager.removeTimedEvent(event);
                           EventManager.updateEventStorage();
                           EventManager.Logger.error("Timed event " + event.onEventFunctionName + " could not be repeated: Repeat time null!");
                       } else {
                           let newevent = new TimedEvent(event.onEventFunctionName, Date.now() + event.repeatTime, event.parms, event.extension, event.repeat, event.repeatTime);
                           EventManager.addTimedEvent(newevent);
                           EventManager.removeTimedEvent(event);
                           EventManager.updateEventStorage();
                       }
                   } else {
                       EventManager.removeTimedEvent(event);
                       EventManager.updateEventStorage();
                   }
               }
            }
        })
    }


    public static removeAllUserEvents(userID:number){
        EventManager.TimedEvents.forEach(event => {
            let tue = TimedUserEvent.fromTimedEvent(event);
            if(tue != null){
                // is timed user event
                if(tue.userID == userID){
                    tue.remove();
                }
            }

        })
    }

    public static removeUserEvent(userID:number, functionName:string){
        EventManager.TimedEvents.forEach(event => {
            if(event.onEventFunctionName == functionName) {
                let tue = TimedUserEvent.fromTimedEvent(event);
                if (tue != null) {
                    // is timed user event
                    if (tue.userID == userID) {
                        tue.remove();
                    }
                }
            }

        })
    }

    public static removeTimedEvent(timedEvent:TimedEvent){
        EventManager.TimedEvents = EventManager.TimedEvents.filter(obj => obj !== timedEvent);
    }


    private executeEventHandler(event:TimedEvent){
        let success = false;
        EventManager.EventHandlers.forEach(eventHandler => {
            if(event.onEventFunctionName == eventHandler.onEventFunctionName){
                EventManager.Logger.debug("EventLoop fire: "+eventHandler.onEventFunctionName);
                eventHandler.onEvent(event);
                success = true;
                return;
            }
        });
        return success;
    }

    private onShutdown() {
        let count = 1;
        EventManager.Logger.warning("Received SIGINT. Broadcasting shutdown event...");
        EventManager.Logger.debug("Storing event data...");



        let shutdownEvents = EventManager.getHWEvents('shutdown');
        shutdownEvents.forEach(event => {
            try {
                if (event.fromExtension != null)
                    EventManager.Logger.info("Shutdown [" + count + "|" + shutdownEvents.length + "]: Extension " + event.fromExtension + "...");
                else
                    EventManager.Logger.info("Shutdown [" + count + "|" + shutdownEvents.length + "]...");
                event.onEvent();
            } catch (e) {
                EventManager.Logger.error("Shutdown event error:");
                EventManager.Logger.error(e);
            }
            count++;
        });
        EventManager.Logger.info("Shutdown complete. Bye!");
        process.exit(1);
    }


    public static fireHWEvent(eventName: string) {
        EventManager.Logger.info("EVENT FIRED: " + eventName);
        let events = EventManager.getHWEvents(eventName);
        let count = 1;
        events.forEach(event => {
            EventManager.Logger.info("EVENT " + eventName + " EXECUTING [" + count + "/" + events.length + "]");
            try {
                event.onEvent();
            } catch (e) {
                EventManager.Logger.error("EVENT " + eventName + " FAILED FOR EXTENSION " + event.fromExtension + "!")
                EventManager.Logger.error(e);
            }
            count++;
        })
    }

    public static getHWEvents(eventName): MinimalEvent[] {
        let events: MinimalEvent[] = [];
        EventManager.DisputeEvent.forEach(event => {
            if (event.event == eventName)
                events.push(event);
        });
        return events;
    }

    addShutdownEvent(event: Event) {

    }

    addDiscordEvent(event: Event) {
        // TODO: management
        EventManager.Logger.debug("Registered new Discord event '" + event.event + "' by extension '" + event.fromExtension + "'");
        DiscordClient.getClient().on(event.event, event.onEvent);
    }

    addDisputeEvent(event: MinimalEvent) {
        EventManager.addDisputeEvent(event);
    }

    static addTimedEvent(event: TimedEvent){
        let success = false;
        if(event.onEventFunctionName == null){
            if(event.extension != null)
                EventManager.Logger.error("Cannot add TimedEvent for extension "+ event.extension +": onFunctionName can not be null!");
            else
                EventManager.Logger.error("Cannot add TimedEvent: onFunctionName can not be null!");
            return;
        }

        EventManager.EventHandlers.forEach(_eventHandler => {
            if (_eventHandler.onEventFunctionName == event.onEventFunctionName) {
                EventManager.TimedEvents.push(event);
                EventManager.Logger.debug("Registered new TimedEvent: '"+event.onEventFunctionName+"' by extension "+event.extension + " | REPEAT: "+event.repeat);
                EventManager.updateEventStorage();
                success = true;
                return;
            }
        });
        if(!success)
            EventManager.Logger.warning("Cannot register TimedEvent: '"+event.onEventFunctionName+"' by extension "+event.extension+":: No EventHandler for event exists!");
        return false;
    }

    addEventHandler(eventHandler:EventHandler){
        if(eventHandler.onEventFunctionName == null){
            if(eventHandler.extension != null)
                EventManager.Logger.error("Cannot add EventHandler for extension "+ eventHandler.extension +": onFunctionName can not be null!");
            else
                EventManager.Logger.error("Cannot add EventHandler: onFunctionName can not be null!");
            return;
        }

        EventManager.EventHandlers.forEach(_eventHandler => {
            if (_eventHandler.onEventFunctionName == eventHandler.onEventFunctionName) {
                EventManager.Logger.error("Cannot add EventHandler: Name " + eventHandler.onEventFunctionName + " already registered!");
                return
            }
        });

        EventManager.EventHandlers.push(eventHandler);
        EventManager.Logger.debug("Registered new EventHandler: '"+eventHandler.onEventFunctionName+"' by extension "+eventHandler.extension);
        return;
    }

    static addDisputeEvent(event: MinimalEvent) {
        if (event.fromExtension == undefined)
            EventManager.Logger.debug("Registered new Dispute event '" + event.event + "'");
        else
            EventManager.Logger.debug("Registered new Dispute event '" + event.event + "' by extension '" + event.fromExtension + "'");
        this.DisputeEvent.push(event);

    }
}