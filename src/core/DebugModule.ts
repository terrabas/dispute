import {Extension} from "./Extension";
import {InstallContext} from "./Models/InstallContext";
import {ArgCommand} from "./Models/ArgCommand";
import {DisputeBot} from "./DisputeBot";
import * as os from "os";
import {EventManager} from "./EventManager";
import {TimedEvent} from "./Models/TimedEvent";
import {EventHandler} from "./Models/EventHandler";
import {TimedUserEvent} from "./Models/TimedUserEvent";

export class DebugModule extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new EventTest());
        context.event.registerEventHandler(new EventDebugHandler());
        this.Logger.info(" HELLO THIS IS DEBUG MODULE");

    }
}


export class EventTest extends ArgCommand {
    constructor() {
        super("event", [], "Adds a debug test timed event");
    }

    execute(message, args: any) {

        let event = new TimedEvent("test",Date.now()+10000, null, "debugModule", true, 10000);
        EventManager.addTimedEvent(event);
        let event2 = new TimedUserEvent(message.author.id,"ukf_notifyunassigned",Date.now()+10000, null, "introduction");
        EventManager.addTimedEvent(event2);

    }
}

export class EventDebugHandler extends EventHandler {
    constructor() {
        super("test");
    }

    public onEvent(event:TimedEvent):boolean{
        console.log("RE E E E E");
        return true;
    }

}