import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {PatternCommand} from "../../core/Models/PatternCommand";
import {ArgumentList} from "../../core/Models/ArgumentList";
import * as request from 'request-promise-native';
import * as moment from "moment";

export class e621 extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new showSugar());
        context.command.add(new showRandomSugar());
    }
}

const build = {
    tags: function(input) {
        if (input == null || !input.length) {
            return "order:random";
        }

        return input
            .split(" ")
            .concat("order:random")
            .map((current) => current.trim().toLowerCase())
            .filter((current) => current)
            .join("+");
    },
    query: async function(tags) {
        return request({
            url: "https://e621.net/post/index.json",
            method: "GET",
            qs: { tags: tags, limit: 1 },
            qsStringifyOptions: { encode: false },
            json: true,
            headers: {
                "User-Agent": "Dispute Images/1.0"
            }
        });
    },
    message: async function(results, message) {
        if (results == null || !results.length) {
            return message.channel.send("I couldn't find anything.");
        }

        let result = results[0];

        return message.channel.send({
            embed: {
                title: "Image Search",
                description: result["artist"].map(escape).join(", "),
                image: {
                    url: result["file_url"]
                },
                fields: [
                    { name: "Score", value: result["score"], inline: true },
                    { name: "Favourites", value: result["fav_count"], inline: true }
                ],
                footer: {
                    text: `Posted ${moment.unix(result["created_at"]["s"]).fromNow()}`
                }
            }
        });
    }
};


export class showRandomSugar extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "show me a random image",
            "gimme some sugar",
            "sugar"
        ]);
    }

    execute(message, args: ArgumentList) {
        build.message(build.query(build.tags(null)), message);
    }

}

export class showSugar extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "show me a random image with {tags}",
            "gimme some sugar with {tags}",
            "sugar with {tags}"
        ]);
    }

    execute(message, args: ArgumentList) {
        build.message(build.query(build.tags(args.getArgument('tags').get())), message);
    }

}