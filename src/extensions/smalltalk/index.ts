import {Extension} from "../../core/Extension";
import {InstallContext} from "../../core/Models/InstallContext";
import {PatternCommand} from "../../core/Models/PatternCommand";
import {ArgumentList} from "../../core/Models/ArgumentList";
import * as responses from "./responses.json";
import * as enums from "./enums.json";
import * as util from "util";
import * as hash from "string-hash";
import * as moment from "moment";
import {InfoCommand} from "../../core/CoreModule";

const random = require('random-item');

export class smalltalk extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new Greeting());
        context.command.add(new Farewell());
        context.command.add(new WhatDoYouThinkOf());
        context.command.add(new AboutYourself());
        context.command.add(new AboutChannel());
        context.command.add(new AboutGuild());
        context.command.add(new Roll());
    }
}

export class Greeting extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super(["hi", "hello", "howdy", "greetings", "yo"]);
    }

    execute(message, args: ArgumentList) {
        message.channel.send(random(responses.greetings));
    }
}

export class Farewell extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super(["bye", "goodbye", "laters", "cya"]);
    }

    execute(message, args: ArgumentList) {
        message.channel.send(random(responses.farewells));
    }
}

export class WhatDoYouThinkOf extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "what do you think of {topic}",
            "what's your opinion of {topic}",
            "is {topic} good",
            "are {topic} good"
        ], "Ask the bot for his opinion", "opinion");
    }

    execute(message, args: ArgumentList) {
        message.channel.send(util.format(responses.feelings[hash(args.getArgument('topic').get()) % responses.feelings.length], args.getArgument('topic').get()));
    }
}

export class Roll extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "roll {user}",
            "please roll {user}",
            "bump {user}"
        ], "Roll an unsuspecting fur", "roll");
    }

    execute(message, args: ArgumentList) {
        message.channel.send(util.format("I rolled %s "+responses.rolling[hash(args.getArgument('user').get()) % responses.rolling.length], args.getArgument('user').get()));
    }
}

export class AboutYourself extends PatternCommand {
    defaultAllow: true;
    constructor() {
        super([
            "tell me about yourself",
            "who are you",
            "about"
        ], "Try it", "aboutyourself");
    }

    execute(message, args: ArgumentList) {
        new InfoCommand().execute(message, args);
    }
}

export class AboutChannel extends PatternCommand {
    constructor() {
        super([
            "tell me about this channel",
            "what is this channel",
            "about channel"
        ]);
    }

    execute(message, args: ArgumentList) {
        message.channel.send({
            embed: {
                title: message.channel.name,
                description: message.channel.topic,
                fields: [
                    {name: "Identifier", value: message.channel.id, inline: true},
                    {name: "Type", value: enums.types[message.channel.type], inline: true},
                    {
                        name: "Category",
                        value: message.channel.parent != null ? message.channel.parent.name : "None",
                        inline: true
                    },
                    {name: "Members", value: message.channel.members.size, inline: true},
                    {name: "NSFW", value: message.channel.nsfw ? "Yes" : "No", inline: true}
                ],
                footer: {
                    text: `Created ${moment(message.channel.createdAt).fromNow()}`
                }
            }
        });
    }
}

export class AboutGuild extends PatternCommand {
    constructor() {
        super([
            "tell me about this guild",
            "what is this guild",
            "about guild"
        ]);
    }

    execute(message, args: ArgumentList) {
        // @ts-ignore
        message.channel.send({
            embed: {
                title: message.guild.name,
                thumbnail: {
                    url: message.guild.iconURL
                },
                fields: [
                    {name: "Identifier", value: message.guild.id, inline: true},
                    {name: "Region", value: enums.regions[message.guild.region], inline: true},
                    {name: "Members", value: message.guild.members.size, inline: true},
                    {name: "Roles", value: message.guild.roles.size, inline: true},
                    {name: "Channels", value: message.guild.channels.size, inline: true},
                    {
                        name: "Verification Level",
                        value: enums.verificationLevels[message.guild.verificationLevel],
                        inline: true
                    },
                    {name: "Content Filter", value: enums.filters[message.guild.explicitContentFilter], inline: true}
                ],
                footer: {
                    text: `Created ${moment(message.guild.createdAt).fromNow()}`
                }
            }

        });
    }
}