import * as ytdl from 'ytdl-core';
import * as request from 'request-promise-native';

export class Item {
    constructor(public url:string){

    }

    static valid(url:string){
        return /(youtube\.com|youtu\.be)|(mp3|mp4|m4a|wav|ogg)$/i.test(url);
    }

    transform(){
        if (/youtube\.com|youtu\.be/i.test(this.url)) {
            console.log(this.url);
            return ytdl(this.url, { filter: "audioonly" });
        }

        return request({
            url: this.url,
            method: "GET",
            headers: {
                "User-Agent": "Dispute Voice/1.0"
            }
        });
    }
}