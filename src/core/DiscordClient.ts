import * as discordjs from 'discord.js';
import {Log} from "./Log";
import {EventManager} from "./EventManager";
// @ts-ignore
import {Intents} from "discord.js";

export class DiscordClient{
    static Discord:discordjs.Client;
    Logger:Log;
    constructor(){
        this.Logger = new Log("DiscordClient");
        const intents = new Intents([
            Intents.NON_PRIVILEGED, // include all non-privileged intents, would be better to specify which ones you actually need
            "GUILD_MEMBERS", // lets you request guild members
        ]);
        // @ts-ignore
        DiscordClient.Discord = new discordjs.Client({ ws: { intents } });
        DiscordClient.Discord.on('error',this.onError.bind(this));
        DiscordClient.Discord.on('disconnect',this.onDisconnect.bind(this));
        DiscordClient.Discord.on('ready',this.onReady.bind(this));
    }

    public static getClient(){
        return DiscordClient.Discord;
    }

    connect(token){
        DiscordClient.Discord.login(token).catch(err => {this.Logger.error("Connection failed: "+err); this.Logger.error("Cannot continue!"); process.exit(1)});
        EventManager.fireHWEvent("connect");
    }

    onError(error){
        this.Logger.error(error);
    }

    onDisconnect(){
        this.Logger.warning("Disconnected!");
        EventManager.fireHWEvent("disconnected");
    }

    onReady(){
        this.Logger.info("Connection established!");
        EventManager.fireHWEvent("ready");

    }

}