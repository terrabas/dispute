import {Item} from "./item";
import {Log} from "../../core/Log";

export class Player {
    dispatcher = null;
    playlist = [];
    Logger:Log;

    constructor(public connection) {
        this.Logger = new Log("STREAMER-PLAYER");
    }

    start(){
        if (this.dispatcher != null || !this.playlist.length) {
            return;
        }

        let stream = this.playlist.shift().transform();

        stream.on("error", (error) => {this.stop(); this.Logger.error("Failed to play YT stream: "+error);this.Logger.error(error.stack)});

        this.dispatcher = this.connection.playStream(stream, { seek: 0, volume: 1 });

        this.dispatcher.on("error", (error) => {this.stop(); this.Logger.error("Failed to play stream: "+error)});
        this.dispatcher.on("end", () => {this.repeat();this.Logger.debug("ended")});


    }

    stop(){
        if (this.dispatcher == null) {
            return;
        }

        this.dispatcher.end();
        this.dispatcher = null;
    }

    repeat(){
        this.stop();
        this.start();
    }

    disconnect(){
        this.connection.disconnect();

        this.connection = null;
        this.dispatcher = null;

        this.playlist = [];
    }

    queue(url:Item){
        this.playlist.push(url);
    }

}