import {Log} from "../Log";
import {APIManager} from "./APIManager";
import {UIPage} from "../Models/UIPage";

export class UIManager {
    private Logger: Log;
    private MainPages: UIPage[] = [];
    private SubPage: string[];

    constructor(public api: APIManager) {
        this.Logger = new Log("WEBUI");
        this.Logger.debug("WebUI initializing");
    }

    public addMainPage(mainPage: UIPage) {
        if (mainPage.extension != null)
            this.Logger.debug("Adding new UI page " + mainPage.pagetitle + " for extension " + mainPage.extension);
        this.MainPages.push(mainPage);
        this.addUIBackend(mainPage);
    }

    public addSubPage(mainPageName:string, subPage: UIPage) {

    }

    private addUIBackend(mainPage: UIPage){
        this.api.registerExtAPICall("ui/"+mainPage.pagetitle+"/index", async (body) => {
            if(mainPage.drawHTML)
                return await mainPage.onDrawHTML();
            return await mainPage.onDraw();
        })
    }

    public getMainPages(): UIPage[] {
        return this.MainPages;
    }

    public getMainPageTitles(): string[] {
        let titles = [];
        this.MainPages.forEach((page) => {
            titles.push(page.pagetitle)
        });
        return titles;
    }
}