import {EventManager} from "../EventManager";
import {TimedEvent} from "./TimedEvent";

export class TimedUserEvent extends TimedEvent {

    constructor(public userID, onEventFunctionName:string, onTime:number, parms:any, extension:string, repeat:boolean = false, repeatTime:number = null) {
        super(onEventFunctionName, onTime, parms, extension, repeat, repeatTime);
        if(parms == null){
            parms = {};
        }
        parms['userID'] = userID;
    }

    public static fromTimedEvent(te:TimedEvent):TimedUserEvent{
        if(te.parms['userID'] != null){
            return new TimedUserEvent(te.parms['userID'], te.onEventFunctionName, te.onTime, te.parms, te.extension, te.repeat, te.repeatTime);
        } else
            return null
    }

}
