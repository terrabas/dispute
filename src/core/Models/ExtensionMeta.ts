export class ExtensionMeta {
    name:string = "unknown";
    author:string = "unknown";
    version:string = "N/A";
    url:string = null;
    initialConfig:string = null;
    constructor(json){
        this.name = json.name;
        this.author = json.author;
        this.version = json.version;
        this.initialConfig = json.initialConfig;
    }
}