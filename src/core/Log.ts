import colors = require("colors");
import {Logger} from "ts-log-debug/lib";
import {DisputeBot} from "./DisputeBot";

export class Log {
    logger;
    static buffer = "- Dispute Discord Bot by Terra -";

    constructor(public moduleName: string) {
        this.logger = new Logger(moduleName);

        this.logger.appenders
            .set("file-log", {
                type: "file", maxLogSize: 10, levels: ["info", "error", "fatal", "warn"], filename: "logfile.log",
                layout: {
                    type: "pattern",
                    pattern: "%d %p %m"
                }
            })
    }

    debug(message: string) {
        if (DisputeBot.debugModeEnabled()) {
            this.logger.debug("[" + this.moduleName + "]: " + message);
            console.log("[" + this.moduleName + "]: " + colors.white(message));
            Log.buffer += "\n["+new Date().toISOString()+"][DEBUG][" + this.moduleName + "]: " + message;
        }
    }

    verbose(message: string) {
        if (DisputeBot.debugModeEnabled()) {
            this.logger.debug("[" + this.moduleName + "]: " + message);
            console.log("[" + this.moduleName + "]: " + colors.gray(message));
            Log.buffer += "\n["+new Date().toISOString()+"][VERBOSE][" + this.moduleName + "]: " + message;
        }
    }

    info(message: string) {
        this.logger.info("[" + this.moduleName + "]: " + message);
        console.log("[" + this.moduleName + "]: " + colors.cyan(message));
        Log.buffer += "\n["+new Date().toISOString()+"][INFO][" + this.moduleName + "]: " + message;
    }

    success(message: string) {
        this.logger.info("[" + this.moduleName + "]: " + message);
        console.log("[" + this.moduleName + "]: " + colors.green(message));
        Log.buffer += "\n["+new Date().toISOString()+"][SUCCESS][" + this.moduleName + "]: " + message;
    }

    /*
    startupinfo(message:string){
        console.log("["+this.moduleName+"] -> "+colors.green(message));
    }

    twotabbedlog(message:string){
        console.log("      "+colors.green(message));
    } */

    warning(message: string) {
        this.logger.warn("[" + this.moduleName + "]: " + message);
        console.log("[" + this.moduleName + "]: " + colors.yellow(message));
        Log.buffer += "\n["+new Date().toISOString()+"][WARNING][" + this.moduleName + "]: " + message;
    }

    error(message: string) {
        this.logger.error("[" + this.moduleName + "]: " + message);
        console.log("[" + this.moduleName + "]: " + colors.red(message));
        Log.buffer += "\n["+new Date().toISOString()+"][ERROR][" + this.moduleName + "]: " + message;
    }

    panic(message: string) {
        this.logger.fatal("PANIC [" + this.moduleName + "]: " + message);
        console.log(colors.bgRed("PANIC [" + this.moduleName + "]: " + message));
        Log.buffer += "\n["+new Date().toISOString()+"][!PANIC!][" + this.moduleName + "]: " + message;
    }

}