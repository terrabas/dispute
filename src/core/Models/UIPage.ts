export class UIPage {
    public drawHTML = true;
    public extension:string = null;


    constructor(public pagetitle: string,) {

    }

    onDrawHTML(): Promise<string> {
        return Promise.resolve("<strong>NOT IMPLEMENTED</strong><br>Please extend the onDrawHTML in your UIPage class.");
    }

    onDraw(): Promise<string> {
        throw "Not implemented!";
    }

}