import {TimedEvent} from "./TimedEvent";

export class EventHandler {

    public extension:string;

    constructor(public onEventFunctionName:string) {

    }

    public onEvent(event:TimedEvent):boolean{
        return true;
    }

}
