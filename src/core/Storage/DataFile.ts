import * as fs from "fs";
import {ConfigManager} from "../ConfigManager";

export enum Encoding {
    RAW,
    JSON,
    BSON
}

export class DataFile {

    private contents = "";
    private parsed = {};

    constructor(private storMan, private extension, private location = "", private name, private encoding:Encoding){

    }

    public read(){
        let data = this.storMan.getDataByLocation(this.name);
        //console.log("RD: "+data);
        switch (this.encoding) {
            case Encoding.JSON:
                this.parsed = JSON.parse(data);
                break;
            default:
                this.contents = data;
        }
    }

    public write(){
        //console.log("WR: "+JSON.stringify(this.parsed));
        //console.log("WR: "+this.contents);
        //console.log("ENCODING: "+this.encoding);
        if(this.encoding == Encoding.JSON)
            return fs.writeFileSync(ConfigManager.rootPath + '/data/'+ this.name+".dtf",JSON.stringify(this.parsed));
        else
            return fs.writeFileSync(ConfigManager.rootPath + '/data/'+ this.name+".dtf",this.contents);
    }

    public get(token = null):any{
        if(token == null)
            return this.parsed;
        return this.parsed[token];
    }

    public set(token, data, andSave = false){
        this.parsed[token] = data;
        if(andSave)
            this.write();
    }

    public getRAW():any{
        return this.contents;
    }

    public setRAW(andSave = false){

    }

    public setEncoding(encoding:Encoding){
        this.encoding = encoding;
    }



}