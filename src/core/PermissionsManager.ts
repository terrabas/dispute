import {Log} from "./Log";
import {BaseCommand} from "./Models/BaseCommand";
import {DisputeBot} from "./DisputeBot";
import {DiscordClient} from "./DiscordClient";
import {MinimalEvent} from "./Models/MinimalEvent";

export class PermissionsManager {
    Logger: Log;
    DiscordUsers = {};

    constructor() {
        this.Logger = new Log("PermMan");
        if (DisputeBot.config['disablePerms'] != null) {
            if (DisputeBot.config['disablePerms'] == true) {
                this.Logger.warning("\n! ! !  W A R N I N G  ! ! ! \n" +
                    "  The permission manager is disabled by config.\n" +
                    "  Permissions will not be checked and all commands are allowed for all users!\n" +
                    "! ! !  W A R N I N G  ! ! !");
                return;
            }
        }

        DisputeBot.EventMan.addDisputeEvent(new MinimalEvent("ready", () => {
            this.Logger.debug("Starting permission sync loop...");
            this.syncDiscordRoles();
            // Every 30 minutes
            setInterval(() => this.syncDiscordRoles(), 1800000);
        }));


    }

    // Command: defaultAllow = true;

    public syncDiscordRoles() {
        this.Logger.info("Syncing discord permission groups...");

        // @ts-ignore
        DiscordClient.getClient().guilds.fetch(DisputeBot.config['guildId']).then((guild) => {
            this.Logger.debug("PERMISSION GUILD: " + guild.name);
            this.Logger.info("Updating permissions...");
            this.DiscordUsers = {};

            guild.members.fetch().then((members) => {
                members.forEach((member) => {
                    this.Logger.debug("Updating member " + member.displayName + ", ID " + member.id);
                    this.DiscordUsers[member.id] = [];

                    member.roles.cache.forEach((role) => {
                        this.Logger.debug("  Assigned role: " + role.name);
                        this.DiscordUsers["" + member.id].push(role.name);
                    })


                });
                this.Logger.debug(JSON.stringify(this.DiscordUsers));
                this.Logger.info("Permission update complete!");

            })
        });

    }

    checkPerm(user, command: BaseCommand): boolean {
        if (DisputeBot.config['disablePerms'] != null) {
            if (DisputeBot.config['disablePerms'] == true) {
                return true;
            }
        }
        // check for custom rules
        if (this.getCustomRules(command) != null) {
            // Check command
            // TODO
        } else {
            // Allow command for everyone by default
            if (command.defaultAllow == true)
                return true;
            else {
                let perm = this.getUserPermissions(user);
                this.Logger.debug('Perm: ' + JSON.stringify(perm));
                // If author has "*", he can do ANYTHING
                if (perm.indexOf('*') != -1)
                    return true;
                if (command.permission == "" || command.permission == null)
                    return false;
                // Author does not have the global-all rule. Checking for command perm
                if (perm.indexOf((command.fromExtension + "." + command.permission).toLowerCase()) != -1)
                    return true;
                this.Logger.debug("Perm check failed! Required: " + (command.fromExtension + "." + command.permission).toLowerCase() + " but user only has " + JSON.stringify(perm));
                return false;
            }
        }
        return false;
    }

    private getRoles() {
        if (DisputeBot.permissions != undefined)
            return DisputeBot.permissions['roles'];
        else
            return [];
    }

    private getRules() {
        if (DisputeBot.config['permissions'] != undefined)
            return DisputeBot.config['permissions']['rules'];
        else
            return [];
    }

    private getUserPermissionsConfig() {
        if (DisputeBot.config['permissions'] != undefined)
            return DisputeBot.config['permissions']['users'];
        else
            return [];
    }

    getUserPermissions(user) {

        if (user == undefined) {
            this.Logger.warning("Undefined user given!");
            return null;
        }

        let userRoles = this.DiscordUsers[user.id];

        this.Logger.debug("User roles: " + JSON.stringify(userRoles));

        let perms = [];

        let permRoles = this.getRoles();

        userRoles.forEach((role) => {
            for (const key in permRoles) {
                let permRole = permRoles[key];
                if (permRole.sync != undefined && permRole.sync == true) {
                    if (key == role) {
                        this.Logger.debug("KEY: " + key + " ROLE: " + role);
                        this.Logger.debug("PERMS: " + JSON.stringify(permRole.perms));
                        permRole.perms.forEach((rolePerm) => {
                            perms.push(rolePerm.toLowerCase());
                        })
                    }
                }
            }

        });


        //this.resolveRoleInherits(roles, role);

        return perms;
    }

    resolveRoleInherits(roles: [], role: String) {
        this.Logger.debug("RESOLVE: " + JSON.stringify(roles));

        roles.forEach((value) => {
            this.Logger.debug(JSON.stringify(value));

            if (value["inherit"] != undefined) {
                this.Logger.debug(value + " inherits" + value["inherit"])

            }
        })
    }

    getCustomRules(command: BaseCommand) {
        let getRul = this.getRules();
        if (getRul == undefined)
            return null;
        let rules = this.getRules()[command.category];
        if (rules == undefined)
            return null;
        else
            return rules;
    }
}