import {Log} from "./Log";
import * as fs from "fs";
import * as path from "path";
import {ExtensionsManager} from "./ExtensionsManager";
import {DisputeBot} from "./DisputeBot";

export class ConfigManager {
    static Logger: Log;
    static rootPath: string = path.dirname(__dirname).split(path.sep).pop();

    static configurations = [];

    constructor(public ExMan: ExtensionsManager) {
        ConfigManager.Logger = new Log("ConfMan");
        if (ConfigManager.checkConfDirExists())
            this.checkConfigs();
        else
            this.createConfigs();

        ConfigManager.Logger.info("Configuration manager loaded");
    }

    public getConfFor(extension): any {
        if(extension == "core" || extension == "permissions") {
            let conf = ConfigManager.configurations[extension];
            //conf['token'] = "THIS INFORMATION HAS BEEN HIDDEN FROM BOT COMMANDS";
            return Object.assign({},conf);
        }
        if(ConfigManager.configurations[extension] == null)
            return null;
        return Object.assign({},ConfigManager.configurations[extension]);
    }

    public static getRootPath(): string {
        return this.rootPath;
    }

    static checkConfDirExists(): boolean {
        if (fs.existsSync(this.rootPath + '/config'))
            return true;
        return false;
    }

    checkConfigs() {
        ConfigManager.Logger.debug("Config path exists. Loading configs...");
        // Dispute config
        this.loadConfig('core');
        this.loadConfig('permissions');
        // Extension configs
        this.ExMan.getExtensionPaths().forEach(path => {
            this.loadConfig(path)
        })
    }

    reloadAllConfigs() {
        this.loadConfig('core');
        this.loadConfig('permissions');
        this.ExMan.getExtensionPaths().forEach(path => {
            this.loadConfig(path)
        });
        DisputeBot.config = DisputeBot.ExMan.ConfMan.getConfFor('core');
        DisputeBot.permissions = DisputeBot.ExMan.ConfMan.getConfFor('permissions');
    }

    public reloadPermissionConfig(){
        this.loadConfig('permissions');
        DisputeBot.permissions = DisputeBot.ExMan.ConfMan.getConfFor('permissions');
    }

    loadConfig(configName) {
        if(!this.requiresConfigFile(configName))
            return;
        // Check if config exists and add it, ie new extension added
        if (!fs.existsSync(ConfigManager.getRootPath() + "/config/" + configName + ".json")) {
            this.copyExtensionConfig(configName);
        }
        try {
            ConfigManager.configurations[configName] = JSON.parse(fs.readFileSync(ConfigManager.getRootPath() + "/config/" + configName + ".json").toString());
        } catch (e) {
            if (configName == 'core') {
                ConfigManager.Logger.panic("Core config load error!");
                ConfigManager.Logger.panic(e.message);
                process.exit(1);
            } else {
                ConfigManager.Logger.error("Failed to load config '" + configName + "'!");
                this.ExMan.disableExtension(configName);
            }
        }
    }

    static writeConfig(configName){
        if(!""+configName in ConfigManager.configurations)
            return false;
        fs.writeFileSync(ConfigManager.getRootPath() + "/config/" + configName + ".json", JSON.stringify(ConfigManager.configurations[configName], null, 4));
        return true;
    }

    requiresConfigFile(extensionPath: string): boolean {
        if(extensionPath == 'core' || extensionPath == 'permissions')
            return true;
        let meta = this.ExMan.getExtensionMeta(extensionPath);
        if (meta == null)
            return false;

        if (meta.initialConfig == null) {
            ConfigManager.Logger.debug("Extension '" + meta.name + "' requires no config.");
            return false;
        } else {
            return true;
        }
    }

    createConfigs() {
        ConfigManager.Logger.warning("Dispute configuration files missing. Creating initial configs...");
        fs.mkdirSync(ConfigManager.rootPath + '/config');
        ConfigManager.Logger.debug("Copying core config...");
        ConfigManager.copyCoreConfig();
        ConfigManager.copyPermConfig();
        ConfigManager.Logger.debug("Copying extension configs...");
        let extensionPaths = this.ExMan.getExtensionPaths();
        if (extensionPaths.length <= 0) {
            ConfigManager.Logger.debug("No extensions found to set up!");
        } else {
            extensionPaths.forEach(path => {
                    this.copyExtensionConfig(path);
                }
            );
            ConfigManager.Logger.info("Configuration files for Dispute and " + extensionPaths.length + " extension(s) created!");
        }


    }

    static copyCoreConfig() {
        if(!fs.existsSync(this.rootPath + '/resources')) {
            ConfigManager.Logger.error("Could not copy core config: resources missing!\n      Check your Dispute installation!");
            process.exit(1);
        }
        fs.copyFileSync(this.rootPath + '/resources/initial-config.json', this.rootPath + '/config/core.json');
    }

    static copyPermConfig() {
        if(!fs.existsSync(this.rootPath + '/resources')) {
            ConfigManager.Logger.error("Could not copy permissions config: resources missing!\n      Check your Dispute installation!");
            process.exit(1);
        }
        fs.copyFileSync(this.rootPath + '/resources/initial-permissions.json', this.rootPath + '/config/permissions.json');
    }

    copyExtensionConfig(extensionPath: string) {
        if (extensionPath == 'core') {
            ConfigManager.Logger.debug("Copying core config...");
            ConfigManager.copyCoreConfig();
            ConfigManager.copyPermConfig();
            return;
        }
        let meta = this.ExMan.getExtensionMeta(extensionPath);
        if (meta == null)
            return;

        try {
            if (!this.requiresConfigFile(extensionPath)) {
                return;
            }
            fs.copyFileSync(ConfigManager.rootPath + '/extensions/' + extensionPath + meta.initialConfig, ConfigManager.rootPath + '/config/' + meta.name + '.json');
            ConfigManager.Logger.info("Created new config for extension '" + meta.name + "'");
        } catch (e) {
            ConfigManager.Logger.error("Failed to copy initial config for extension '" + meta.name + "'! Unable to load extension");
        }

    }

}