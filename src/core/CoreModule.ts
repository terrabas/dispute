import {Extension} from "./Extension";
import {Log} from "./Log";
import {InstallContext} from "./Models/InstallContext";
import {ArgCommand} from "./Models/ArgCommand";
import {Argument} from "./Models/Argument";
import {DisputeBot} from "./DisputeBot";
import {ArgumentList} from "./Models/ArgumentList";
import * as os from "os";
import * as moment from "moment";
import * as credits from "../resources/credits.json";
import {PatternCommand} from "./Models/PatternCommand";
import {ConfigManager} from "./ConfigManager";
import {EventManager} from "./EventManager";
import {PermissionsManager} from "./PermissionsManager";

export class CoreModule extends Extension {
    setup(context: InstallContext) {
        super.setup(context);
        context.command.add(new InfoCommand());
        context.command.add(new ListCommands());
        context.command.add(new ExtensionCommand());
        context.command.add(new ConfigCommand());
        context.command.add(new DebugCommand());
        context.command.add(new PermCommand());
        let df = context.storage.requestDataFile("PERMDTA");
        df.write();
    }
}

export class ExtensionCommand extends ArgCommand {
    constructor() {
        super("extensions", [new Argument(true, 'operation', 'list, enable, disable')], "Interact with the extension manager");
    }

    execute(message, args: ArgumentList) {
        switch (args.getArgument("operation").get()) {
            case 'list':
                let content = "__**Extension list**__\n";
                let exList = DisputeBot.ExMan.getExtensions();
                let exDisList = DisputeBot.ExMan.getDisabledExtensions();
                content = content + "Total of " + exList.length + " extensions registered\n";
                content = content + "Total of " + exDisList.length + " extensions disabled\n\n";
                exList.forEach(value => {
                    content = content + "> `" + value.meta.name + "` -- ENABLED\n";
                });
                content = content + "\n";
                exDisList.forEach(value => {
                    content = content + "> `" + value + "` -- DISABLED\n";
                });

                message.channel.send(content);
                break;
            case 'restart':
                if(DisputeBot.Watchdog == null){
                    message.channel.send(">>> Error: No watchdog running!");
                    return;
                }
                DisputeBot.Watchdog.restart();
                EventManager.fireHWEvent("shutdown");
                break;
            case 'rescan':
                let origCount = DisputeBot.ExMan.getExtensions().length;
                message.channel.send(">>> Rescanning extensions\n**Please wait...**");
                DisputeBot.ExMan.loadExtensions();
                let newCount = DisputeBot.ExMan.getExtensions().length;
                let nowCount = newCount - origCount;
                if (nowCount < 0)
                    message.channel.send("> Scan complete! " + (nowCount * -1) + " extension(s) removed");
                if (nowCount == 0)
                    message.channel.send("> Scan complete! No new extensions detected");
                if (nowCount > 0)
                    message.channel.send("> Scan complete! " + nowCount + " new extensions loaded");
                break;
            case 'info':
                message.channel.send("__**Extension information**__");
                let extensions = DisputeBot.ExMan.getExtensions();
                if(extensions.length == 0) {
                    message.channel.send("\nNo extensions installed");
                    return;
                }
                else {
                    message.channel.send({
                        embed: {
                            color: 0x00ff00,
                            description: "Enabled extension",
                        }
                    });
                    message.channel.send({
                        embed: {
                            color: 0xff0000,
                            description: "Disabled extension",
                        }
                    });

                    extensions.forEach(extension => {
                        message.channel.send({
                            embed: {
                                color: 0x00ff00,
                                title: extension.meta.name,
                                //description: credits.description.join("\n"),
                                //thumbnail: {
                                 //   url: message.client.user.avatarURL
                                //},
                                fields: [
                                    {name: "Version", value: extension.meta.version, inline: true},
                                    {name: "Author(s)", value: extension.meta.author, inline: true},
                                    //{name: "Website", value: extension.meta.url, inline: true}
                                ]
                            }
                        });
                    });
                    message.channel.send("> Command processing complete");
                }
                break;
            default:
                message.channel.send("ExMan: Unknown command");
        }
    }
}

export class ListCommands extends ArgCommand {
    constructor() {
        super("commands", [], "Lists all currently registered commands", "commands.list");
    }

    execute(message, args: any) {
        let content = "__**Command list**__\n";
        let comList = DisputeBot.CommandHandler.getCommandList(message.author);
        content = content + "Total of " + comList.length + " commands registered\n\n";
        comList.forEach(value => {
            if(value.category != null)
                content = content + "> `" + value.category + "` " + value.description + "\n";
            else {
                content+="> ";
                for (let pattern of value.pattern){
                    content = content + "`" + pattern + "` ";
                }
                if(value.description != undefined)
                    content+=value.description+"\n";
                else
                    content+="\n";
            }
        });

        message.channel.send(content);
    }
}

export class ConfigCommand extends ArgCommand {
    constructor() {
        super("config", [new Argument(true, 'operation', 'show, reload, set, store'), new Argument(false, 'configFileName'),new Argument(false, 'key'),new Argument(false, 'value')], "Interact with the configuration manager");
    }

    execute(message, args: ArgumentList) {
        switch (args.getArgument("operation").get()) {
            case 'show':
                let content = "__**Current configuration**__\n";
                let coreConfig = DisputeBot.ExMan.ConfMan.getConfFor('core');
                coreConfig['token'] = "HIDDEN INFORMATION";
                content += "\n> **CORE config**\n```json\n" + JSON.stringify(coreConfig, null, 4)+ " ```";
                message.channel.send(content);
                content = "";
                DisputeBot.ExMan.getExtensions().forEach(extension => {
                    if(DisputeBot.ExMan.ConfMan.getConfFor(extension.meta.name) != undefined)
                        content += "\n> **" + extension.meta.name + " extension config**\n```json\n" + JSON.stringify(DisputeBot.ExMan.ConfMan.getConfFor(extension.meta.name), null, 4) + " ```";
                    else
                        content += "\n> **" + extension.meta.name + " extension config**\n```json\nThis extension uses no config ```";
                    if(content.length >= 2000){
                        message.channel.send("\n> **" + extension.meta.name + " extension config**\n```json\nThis config is too long to be displayed in a message ```");
                    } else
                    message.channel.send(content);
                    content = "";

                });
                //content = content + JSON.stringify(Object.keys(DisputeBot.ExMan.ConfMan.confi));
                break;
            case 'reload':
                message.channel.send(">>> Reloading core and extension configs\n**Please wait...**");
                DisputeBot.ExMan.ConfMan.reloadAllConfigs();
                message.channel.send("> Reload complete!");
                break;
            case 'set':
                    if(args.getArgument('configFileName').get() == null || args.getArgument('key').get() == null || args.getArgument('value').get() == null) {
                        message.channel.send("__Syntax error__");
                        return;
                    }
                    if(!""+args.getArgument("configFileName").get() in ConfigManager.configurations){
                        message.channel.send("__Config name invalid__");
                        return;
                    }
                    ConfigManager.configurations[args.getArgument("configFileName").get()][args.getArgument('key').get()] = args.getArgument('value').get();
                    message.channel.send("> Successfully set '"+args.getArgument("configFileName").get()+"' config parameter `"+args.getArgument('key').get()+"` to `"+args.getArgument('value').get()+"`");
                break;
            case 'store':
            case 'commit':
                if(args.getArgument("configFileName").get() == null){
                    message.channel.send("__Syntax error__");
                    return;
                }
                if(ConfigManager.writeConfig(args.getArgument("configFileName").get()))
                    message.channel.send("> Configuration for `"+args.getArgument("configFileName").get()+"` committed to disk!");
                else
                    message.channel.send("__Config write error__");
                break;
            default:
                message.channel.send("ConfMan: Unknown command");
        }
    }
}

export class DebugCommand extends ArgCommand {
    constructor() {
        super("debug", [], "Provides debug information about the system");
    }

    execute(message, args: any) {
        let debugM = "disabled";
        if(DisputeBot.config['debug'] == true){
            debugM = "enabled";
        }
        message.channel.send({
            embed: {
                title: "Debug Information",
                fields: [
                    {name: "Server", value: os.hostname(), inline: true},
                    {name: "Platform", value: `${os.platform()}/${os.arch()}`, inline: true},
                    {name: "Version", value: `Node ${process.version}`, inline: true},
                    {name: "Uptime", value: moment.duration(process.uptime(), "seconds").humanize(), inline: true},
                    {name: "Debug Mode", value: debugM, inline: true}
                ]
            }
        });
    }
}

export class InfoCommand extends ArgCommand {
    constructor() {
        super("info", [], "Provides information about Dispute");
    }

    execute(message, args: any) {
        message.channel.send({
            embed: {
                title: credits.name,
                description: credits.description.join("\n"),
                thumbnail: {
                    url: message.client.user.avatarURL
                },
                fields: [
                    {name: "Authors", value: credits.authors.join(", "), inline: true},
                    {name: "Contributors", value: credits.contributors.join(", "), inline: true}
                ]
            }
        });

    }
}


export class PermCommand extends ArgCommand {
    constructor() {
        super("perm", [new Argument(true, "action","reload / sync / add / remove / self")], "Interact with the permission manager");
    }

    execute(message, args: any) {
        switch (args.getArgument("action").get()) {
            case 'sync':
                message.channel.send("> Discord permission sync queued");
                DisputeBot.CommandHandler.getPermMan().syncDiscordRoles();
                message.channel.send("> Permission sync complete!");
                break;
            case 'reload':
                message.channel.send("> Reloading permissions...");
                DisputeBot.ExMan.ConfMan.reloadPermissionConfig();
                message.channel.send("> Permissions reload complete!");
                break;
            case 'self':
                message.channel.send("> You currently have the following permissions: ");
                message.channel.send("> "+(DisputeBot.CommandHandler.getPermMan().getUserPermissions(message.author).join(', ')));
                if(DisputeBot.CommandHandler.getPermMan().getUserPermissions(message.author).indexOf('*') != -1)
                    message.channel.send("> According to your permissions, you must be pretty important!");
                break;
            default:
                message.channel.send("> Unknown command! ");


        }

    }
}