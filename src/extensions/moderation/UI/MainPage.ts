import {UIPage} from "../../../core/Models/UIPage";
import {ModDatabase} from "../core/ModDatabase";
import {DiscordClient} from "../../../core/DiscordClient";
import {ModEntry} from "../models/ModEntry";

export class MainPage extends UIPage {

    constructor(){
        super("Moderation")
    }

    async onDrawHTML(): Promise<string> {
        let header = "<h4>Welcome to the moderation panel!</h4>";
        let content = header;
        content += "<h5>Moderation entries</h5>";

        let ids = Object.keys(ModDatabase.userData.get());
        for (let i = 0; i < ids.length; i++) {
            let user = await DiscordClient.getClient().fetchUser(ids[i]);
            content += "# USERDATA ID " + ids[i] + " AKA " + user.username + "<br>";
            content += "uid:" + ids[i] + "<br>";
            content += "# Timestamp, Type, Until, Issued by, Issued by ID, Issued for, reason<br>";
            ModDatabase.getModEntries(user).forEach(function (modData: ModEntry) {
                content += "\"" + modData.timestamp + "\",\"" + modData.type + "\",\"" + modData.until + "\",\"" + modData.issuedBy + "\",\"" + modData.issuedByID + "\",\"" + modData.issuedFor + "\",\"" + modData.reason + "\"\n";
            });
            content += "<br><br>";
        }
        return Promise.resolve(content);
    }
}