import {Argument} from "./Argument";

export class ArgumentList {
    private arguments:Argument[];

    constructor(arg:[]){
        this.arguments = arg;
    }

    getArgument(name:string):Argument{
        return this.arguments.find(x => x.name === name) as Argument;
    }
}