import {Log} from "./Log";
import {ConfigManager} from "./ConfigManager";
import * as fs from "fs";
import {ExtensionMeta} from "./Models/ExtensionMeta";
import {Extension} from "./Extension";
import {StorageManager} from "./StorageManager";
import {InstallContext} from "./Models/InstallContext";
import {DisputeBot} from "./DisputeBot";
import {CoreModule} from "./CoreModule";
import {DebugModule} from "./DebugModule";


export class ExtensionsManager {
    Logger: Log;
    ConfMan: ConfigManager;
    StorMan: StorageManager;
    disabledExtensions = [];
    extensions = [];
    coreModule: CoreModule;
    debugModule: DebugModule;


    constructor() {
        this.Logger = new Log("ExMan");
        this.Logger.debug("Instancing ConfMan...");
        this.ConfMan = new ConfigManager(this);
        this.Logger.debug("Instancing StorMan...");
        this.StorMan = new StorageManager();
    }

    getExtensions(): any {
        this.Logger.debug(JSON.stringify(this.extensions));
        return this.extensions;
    }

    getDisabledExtensions():any {
        return this.disabledExtensions;
    }

    disableExtension(extensionName: string) {
        this.disabledExtensions.push(extensionName);
        this.Logger.warning("Extension '" + extensionName + "' has been disabled!");
    }

    loadExtensions() {
        this.getExtensionPaths().forEach(path => {
            let skip = false;

            if (!this.disabledExtensions.includes(path) && path != "disabled") {
                try {
                    let meta = this.getExtensionMeta(path);
                    // Meta invalid, load impossible
                    if(meta != null){
                        this.Logger.info("Loading extension '" + meta.name + "'...");
                        // Check for duplicates
                        this.extensions.forEach(extension => {
                            if(extension.meta.name == meta.name){
                                this.Logger.warning("Trying to load previously loaded extension '" + meta.name + "'! Skipping...");
                                skip = true;
                            }
                        });
                        if(!skip) {
                            let extension: Extension;
                            let tmpCls;
                            // Load extension entrypoint into memory
                            try {
                                extension = require("./../extensions/" + path + "/index");
                            } catch (e) {
                                this.Logger.error("Extension '" + meta.name + "' load failed: Missing entrypoint file or error during load");
                                this.Logger.error(e);
                                this.disableExtension(path);
                                return;
                            }
                            try {
                                tmpCls = new extension[meta.name](this.ConfMan.getConfFor(path), new Log(meta.name));
                            } catch (e) {
                                this.Logger.error("Extension '" + meta.name + "' load failed: Extension constructor missing or exception thrown\n" + e);
                                this.disableExtension(path);
                                return;
                            }
                            try {
                                tmpCls.setup(this.makeInstallContext(meta.name));
                            } catch (e) {
                                this.Logger.error("Extension '" + meta.name + "' load failed: Setup method threw exception\n" + e);
                                this.disableExtension(path);
                                return;
                            }
                            this.extensions.push({class: tmpCls, "meta": meta, "path": path});
                        }
                    } else {
                        this.disableExtension(path);
                    }

                } catch (e) {
                    this.Logger.error("Failed to load extension: " + e.message);
                    this.Logger.error(e);
                    this.disableExtension(path);
                }
            }
        });
    }

    loadCoreModule() {
        this.coreModule = new CoreModule(this.ConfMan.getConfFor('core'), new Log('CoreModule'));
        this.coreModule.setup(this.makeInstallContext("CORE"));
    }

    loadDebugModule(){
        this.coreModule = new DebugModule(this.ConfMan.getConfFor('debug'), new Log('DebugModule'));
        this.coreModule.setup(this.makeInstallContext("DEBUG"));
    }

    private makeInstallContext(extensionName:string): InstallContext {
        return new InstallContext(DisputeBot.CommandHandler, extensionName, DisputeBot.EventMan);
    }

    getExtensionMeta(extensionPath: string) {
        try {
            let json = JSON.parse(fs.readFileSync(ConfigManager.getRootPath() + "/extensions/" + extensionPath + "/package.json").toString());
            return new ExtensionMeta(json);
        } catch (e) {
            this.Logger.error("Failed to get meta for extension '" + extensionPath + "'! Extension can not be loaded!");
            return null;
        }
    }

    getExtensionPaths(): string[] {
        if (!fs.existsSync(ConfigManager.getRootPath() + "/extensions/")) {
            this.Logger.warning("Extension folder structure invalid! Creating...");
            fs.mkdirSync(ConfigManager.getRootPath() + "/extensions");
        }

        return fs.readdirSync(ConfigManager.getRootPath() + "/extensions/", {withFileTypes: true})
            .filter(dirent => dirent.isDirectory())
            .map(dirent => dirent.name)
    }

}