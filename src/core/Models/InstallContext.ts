import {CommandHandler} from "../CommandHandler";
import {BaseCommand} from "./BaseCommand";
import {EventManager} from "../EventManager";
import {StorageManager} from "../StorageManager";
import {Event} from "./Event";
import {Encoding} from "../Storage/DataFile";
import {EventHandler} from "./EventHandler";
import {TimedEvent} from "./TimedEvent";
import {UIPage} from "./UIPage";
import {DisputeBot} from "../DisputeBot";

export class InstallContext {
    command:CommandContext;
    event:EventContext;
    storage:StorageContext;
    webui:WebUIContext;

    //private extensionName;
    constructor(commandHandler:CommandHandler, extensionName:string, eventManager:EventManager){
        this.command = new CommandContext(commandHandler, extensionName);
        this.event = new EventContext(eventManager, extensionName);
        this.storage = new StorageContext();
        this.webui = new WebUIContext(extensionName);
    }
}

export class CommandContext {
    private commandHandler:CommandHandler;
    private extensionName:string;
    constructor(commandHandler:CommandHandler, extensionName:string) {
        this.commandHandler = commandHandler;
        this.extensionName = extensionName;
    }

    add(command:BaseCommand){
        this.commandHandler.add(command, this.extensionName);
    }
}

export class EventContext {
    private eventHandler:EventManager;
    private extensionName:string;
    constructor(eventManager:EventManager, extensionName:string) {
        this.eventHandler = eventManager;
        this.extensionName = extensionName;
    }

    registerDiscordEvent(event:Event){
        event.fromExtension = this.extensionName;
        this.eventHandler.addDiscordEvent(event);
    }

    registerDisputeEvent(event:Event){
        event.fromExtension = this.extensionName;
        this.eventHandler.addDisputeEvent(event);
    }

    registerEventHandler(eventHandler:EventHandler){
        eventHandler.extension = this.extensionName;
        this.eventHandler.addEventHandler(eventHandler);
    }

    registerTimedEvent(event:TimedEvent){
        event.extension = this.extensionName;
        EventManager.addTimedEvent(event);
    }
}

export class StorageContext {

    requestDataFile(fileName:string, encoding:Encoding = Encoding.RAW){
        return StorageManager.requestDataFile(fileName, encoding);
    }
}

export class WebUIContext {
    private extensionName:string;
    constructor(extensionName:string) {
        this.extensionName = extensionName;
    }

    isAvailable(){
        return DisputeBot.API.isWebUIAvailable();
    }

    addMainPage(page:UIPage){
        if(!this.isAvailable())
            return;
        page.extension = this.extensionName;
        DisputeBot.API.getUIManager().addMainPage(page);
    }

    addSubPage(mainPageTitle:string, page:UIPage){
        if(!this.isAvailable())
            return;
        page.extension = this.extensionName;
        DisputeBot.API.getUIManager().addSubPage(mainPageTitle, page);
    }
}