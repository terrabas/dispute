import {DisputeBot} from "../DisputeBot";
import * as discordjs from "discord.js";
import {MinimalEvent} from "./MinimalEvent";
import {DiscordClient} from "../DiscordClient";

export class Event extends MinimalEvent {
    fromExtension:string;
    client:discordjs.Client = DiscordClient.getClient();
}