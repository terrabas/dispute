import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import LogManager from '../components/LogManager'
import ExtensionsManager from '../components/ExtensionsManager'
import Setup from "../components/Setup";
import ExternalUI from "../components/ExternalUI";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/log-manager',
      name: 'LogManager',
      component: LogManager,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/extensions',
      name: 'ExtensionsManager',
      component: ExtensionsManager,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: '/setup',
      redirect: '/setup/1'
    },
    {
      path: '/setup/:step',
      name: 'Setup',
      component: Setup,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/ext/:navbaritem',
      name: 'External UI',
      component: ExternalUI,
      meta: {
        requiresAuth: false
      }
    }
  ]
})
